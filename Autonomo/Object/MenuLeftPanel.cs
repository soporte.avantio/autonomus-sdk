﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Object
{
    public partial class MenuLeftPanel : Form
    {

        public MenuLeftPanel()
        {
            InitializeComponent();

            Salir.MouseEnter += new EventHandler(Salir_MouseEnter);
            Salir.MouseLeave += new EventHandler(Salir_MouseLeave);
            Igv = "0";
        }

        #region Enumerable
        public enum Theme
        {
            White, Dark, BlueDark, Green
        }
        #endregion

        #region Theme
        public void ThemeStyle(Theme theme)
        {            
            switch (theme)
            {
                case Theme.White:
                    {
                        Color topControlColor = Color.FromArgb(232, 232, 232);
                        Color topControlForeColor = Color.FromArgb(64, 64, 64);
                        SetTopPanelAndTextColor(topControlColor, topControlForeColor);

                        Color backMainColor = Color.FromArgb(247, 249, 252);
                        Color backMainForeColor = Color.Black;
                        Color detailForeColor = Color.Gray;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        Color subContainerColor = Color.White;
                        SubContenedor.BackColor = subContainerColor;
                        break;
                    }
                case Theme.Dark:
                    {
                        Color topControlColor = Color.FromArgb(10, 73, 72); 
                        Color topControlForeColor = Color.White;
                        SetTopPanelAndTextColor(topControlColor, topControlForeColor);

                        Color backMainColor = Color.FromArgb(16, 29, 37);
                        Color backMainForeColor = Color.FromArgb(10, 140, 129); //Color.WhiteSmoke;
                        Color detailForeColor = Color.Gainsboro;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        Color subContainerColor = Color.FromArgb(35, 45, 54);
                        SubContenedor.BackColor = subContainerColor;
                        break;
                    }
                case Theme.BlueDark:
                    {

                        break;
                    }
                case Theme.Green:
                    {
                        break;
                    }
            }
        }
        private void SetLabelButtonColor(Panel panel, Color title, Color detail)
        {
            foreach (System.Windows.Forms.Control c in panel.Controls)
            {
                if (c is Autonomo.Control.LabelButton) 
                {
                    ((Control.LabelButton)c).ColorText = title;
                    ((Control.LabelButton)c).ColorDetailText = detail;
                }
            }
        }

        private void SetPanelBackAndTextColor(Color panelColor, Color titleForeColor, Color detailForeColor)
        {
            Contenedor.BackColor = panelColor;
            PanelModulo.BackColor = panelColor;
            PanelInfo.BackColor = panelColor;
            Arriba.BackColor = panelColor;
            Izquierda.BackColor = panelColor;
            Abajo.BackColor = panelColor;
            Derecha.BackColor = panelColor;

            EmpresaNombre.ForeColor = titleForeColor;
            SubTitulo.ForeColor = titleForeColor;
            UserDisplayText.ForeColor = titleForeColor;
            Caja.ColorText = titleForeColor;
            Venta.ColorText = titleForeColor;
            Cliente.ColorText = titleForeColor;
            Movimiento.ColorText = titleForeColor;
            Compra.ColorText = titleForeColor;
            Proveedor.ColorText = titleForeColor;
            Almacen.ColorText = titleForeColor;
            Reporte.ColorText = titleForeColor;
            Configuracion.ColorText = titleForeColor;

            Caja.ColorDetailText = detailForeColor;
            Venta.ColorDetailText = detailForeColor;
            Cliente.ColorDetailText = detailForeColor;
            Movimiento.ColorDetailText = detailForeColor;
            Compra.ColorDetailText = detailForeColor;
            Proveedor.ColorDetailText = detailForeColor;
            Almacen.ColorDetailText = detailForeColor;
            Reporte.ColorDetailText = detailForeColor;
            Configuracion.ColorDetailText = detailForeColor;

            SetLabelButtonColor(PanelCaja, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelVenta, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelCliente, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelMovimiento, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelCompra, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelProveedor, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelAlmacen, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelReporte, titleForeColor, detailForeColor);
            SetLabelButtonColor(PanelConfiguracion, titleForeColor, detailForeColor);
        }
        private void SetTopPanelAndTextColor(Color panelColor, Color foreColor)
        {
            PanelControl.BackColor = panelColor;
            Detalle.ForeColor = foreColor;
            Software.ForeColor = foreColor;
            Ayuda.ForeColor = foreColor;
            Minimizar.ForeColor = foreColor;
            Salir.ForeColor = foreColor;
        }
        #endregion
        private void InitializeSizeModule()
        {
            PanelCaja.Height = PanelVenta.Height = PanelCliente.Height =
                PanelCompra.Height = PanelProveedor.Height = PanelAlmacen.Height =
                PanelReporte.Height = PanelConfiguracion.Height = 52;
        }

        #region Espacio: Atributos
        public int Alert { get { return int.Parse(Bell.Text); } set { Bell.Text = value.ToString(); } }
        public string Module { get; set; }
        public string Server { get; set; }
        public string DataBase { get; set; }
        public string TCambio { get; set; }
        public string Igv { get; set; }
        public string Usuario { get; set; }
        #endregion

        #region Espacio: Estilos de modulo
        private const int subHeight = 44; // Tamañao del Sub Control --> que llama a los formualrios hijos
        private int mainHeight = 0; // Hace referencia al Control del modulo
        private int espacio = 8;
        private bool displayCaja = false;
        private bool displayVenta = false;
        private bool displayCliente = false;
        private bool displayMovimiento = false;
        private bool displayCompra = false;
        private bool displayProveedor = false;
        private bool displayAlmacen = false;
        private bool displayReporte = false;
        private bool displayConfiguracion = false;
        #endregion

        private void Salir_MouseEnter(object sender, EventArgs e)
        { Salir.ForeColor = Color.WhiteSmoke; }
        private void Salir_MouseLeave(object sender, EventArgs e)
        { Salir.ForeColor = Color.FromArgb(64, 64, 64); }


        #region Espacio: Display modules
        private int GetSubLabelCount(Panel panel)
        {
            int result = 0;
            foreach (System.Windows.Forms.Control c in panel.Controls)
            {
                if (c is Autonomo.Control.LabelButton) { result++; }
            }
            return result;
        }

        private void CajaB_Click(object sender, EventArgs e)
        {
            if (displayCaja)
            {
                PanelCaja.Height = (espacio + mainHeight);
                displayCaja = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelCaja);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelCaja.Height = alturaDelpanel;
            displayCaja = true;
        }
        private void VentaB_Click(object sender, EventArgs e)
        {
            if (displayVenta)
            {
                PanelVenta.Height = (espacio + mainHeight);
                displayVenta = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelVenta);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelVenta.Height = alturaDelpanel;
            displayVenta = true;
        }
        private void ClienteB_Click(object sender, EventArgs e)
        {
            if (displayCliente)
            {
                PanelCliente.Height = (espacio + mainHeight);
                displayCliente = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelCliente);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelCliente.Height = alturaDelpanel;
            displayCliente = true;
        }
        private void MovimientoB_Click(object sender, EventArgs e)
        {
            if (displayMovimiento)
            {
                PanelMovimiento.Height = (espacio + mainHeight);
                displayMovimiento = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelMovimiento);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelMovimiento.Height = alturaDelpanel;
            displayMovimiento = true;
        }
        private void CompraB_Click(object sender, EventArgs e)
        {
            if (displayCompra)
            {
                PanelCompra.Height = (espacio + mainHeight);
                displayCompra = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelCompra);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelCompra.Height = alturaDelpanel;
            displayCompra = true;
        }
        private void ProveedorB_Click(object sender, EventArgs e)
        {
            if (displayProveedor)
            {
                PanelProveedor.Height = (espacio + mainHeight);
                displayProveedor = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelProveedor);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelProveedor.Height = alturaDelpanel;
            displayProveedor = true;
        }
        private void AlmacenB_Click(object sender, EventArgs e)
        {
            if (displayAlmacen)
            {
                PanelAlmacen.Height = (espacio + mainHeight);
                displayAlmacen = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelAlmacen);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelAlmacen.Height = alturaDelpanel;
            displayAlmacen = true;
        }
        private void ReporteB_Click(object sender, EventArgs e)
        {
            if (displayReporte)
            {
                PanelReporte.Height = (espacio + mainHeight);
                displayReporte = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelReporte);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelReporte.Height = alturaDelpanel;
            displayReporte = true;
        }
        private void ConfiguracionB_Click(object sender, EventArgs e)
        {
            if (displayConfiguracion)
            {
                PanelConfiguracion.Height = (espacio + mainHeight);
                displayConfiguracion = false;
                return;
            }

            int numObjectInPanel = GetSubLabelCount(PanelConfiguracion);
            int alturaDelpanel = (numObjectInPanel * subHeight) + (mainHeight + espacio);
            PanelConfiguracion.Height = alturaDelpanel;
            displayConfiguracion = true;
        }
        #endregion
        private void MenuLeftPanel_Load(object sender, EventArgs e)
        {
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;

            Autonomo.Class.RoundObject.RoundPanel(SubContenedor, 20, 20);

            UserDisplayText.Text = $"Bienvenido {Usuario}";
            //Bell.Text = Alert.ToString();

            //Crear Eventos para estilos y manejo de los modulos
            Caja.ButtonClick += new EventHandler(CajaB_Click);
            Venta.ButtonClick += new EventHandler(VentaB_Click);
            Cliente.ButtonClick += new EventHandler(ClienteB_Click);
            Compra.ButtonClick += new EventHandler(CompraB_Click);
            Proveedor.ButtonClick += new EventHandler(ProveedorB_Click);
            Almacen.ButtonClick += new EventHandler(AlmacenB_Click);
            Reporte.ButtonClick += new EventHandler(ReporteB_Click);
            Configuracion.ButtonClick += new EventHandler(ConfiguracionB_Click);
            Movimiento.ButtonClick += new EventHandler(MovimientoB_Click);
            //
            mainHeight = Caja.Height;
            InitializeSizeModule();
        }

        private void Salir_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Miminizar_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void tmrDateTime_Tick(object sender, EventArgs e)
        {
            decimal igv = Math.Round((decimal.Parse(Igv) * 100), 1);
            Detalle.Text = $"Modulo {Module}  :  Conexión {Server}  {DataBase}  :  " +
                $"Tipo de cambio sunat ({TCambio})  :  IGV ({igv}%)  :  {DateTime.Now.ToLongDateString()}  {DateTime.Now.ToLongTimeString()}";
        }

        private void MenuLeftPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                //string result = Autonomus.Class.Message.MessageBox(3,
                //"¡Hasta pronto!. No hay procesos pentientes, puede salir de la Aplicación sin problemas." + Environment.NewLine + Environment.NewLine +
                //"¿Estás seguro de continuar?");
                if ("Yes" == "Yes")
                { Application.Exit(); }
                else
                { e.Cancel.Equals(true); }
            }
        }

        private void MenuLeftPanel_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }
    }
}
