﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Object
{
    public partial class Registry : Form
    {
        public Registry()
        {
            InitializeComponent();
        }

        #region Enumerable
        public enum Theme
        {
            White, Dark, BlueDark, Green
        }
        #endregion
        #region Theme
        public void ThemeStyle(Theme theme)
        {
            switch (theme)
            {
                case Theme.White:
                    {
                        Color topControlColor = Color.FromArgb(247, 249, 252);
                        Color topControlForeColor = Color.FromArgb(64, 64, 64);
                        SetButtonPanelAndTextColor(topControlColor, topControlForeColor,Theme.White );

                        Color backMainColor = Color.White;
                        Color backMainForeColor = Color.FromArgb(64, 64, 64);
                        Color detailForeColor = Color.DimGray;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        SetGridColor(Color.White, Color.FromArgb(247, 249, 252),
                            Color.Black, Color.Black);
                        break;
                    }
                case Theme.Dark:
                    {
                        Color topControlColor = Color.FromArgb(16, 29, 37);
                        Color topControlForeColor = Color.Gainsboro;
                        SetButtonPanelAndTextColor(topControlColor, topControlForeColor, Theme.Dark);

                        Color backMainColor = Color.FromArgb(35, 45, 54); 
                        Color backMainForeColor = Color.FromArgb(10, 140, 129); //Color.WhiteSmoke;
                        Color detailForeColor = Color.Gainsboro;
                        SetPanelBackAndTextColor(backMainColor, backMainForeColor, detailForeColor);

                        SetGridColor(Color.FromArgb(35, 45, 54), Color.FromArgb(16, 29, 37),
                            Color.Gainsboro, Color.WhiteSmoke);
                        SetControlColor();//Colocar parametros si se va a usar en otros temas.
                        break;
                    }
                case Theme.BlueDark:
                    {
                        
                        break;
                    }
                case Theme.Green:
                    {

                        break;
                    }
            }
        }
        private void SetControlColor()
        {
            foreach (System.Windows.Forms.Control c in pnlTopControl.Controls)
            {
                if (c is Autonomo.Control.FlatTextBox)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((Control.FlatTextBox)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((Control.FlatTextBox)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((Control.FlatTextBox)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((Control.FlatTextBox)c).ColorText = Color.WhiteSmoke;
                    ((Control.FlatTextBox)c).ColorFocus = Color.MintCream;
                    ((Control.FlatTextBox)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.Control.FlatFindText)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((Control.FlatFindText)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((Control.FlatFindText)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((Control.FlatFindText)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((Control.FlatFindText)c).ColorText = Color.WhiteSmoke;
                    ((Control.FlatFindText)c).ColorFocus = Color.MintCream;
                    ((Control.FlatFindText)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.Control.FlatComboBox)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((Control.FlatComboBox)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((Control.FlatComboBox)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((Control.FlatComboBox)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((Control.FlatComboBox)c).ColorText = Color.WhiteSmoke;
                    ((Control.FlatComboBox)c).ColorFocus = Color.MintCream;
                    ((Control.FlatComboBox)c).ColorTitle = Color.Gainsboro;
                }
                if (c is Autonomo.Control.FlatDateTime)
                {
                    //Personalizar colores si se va a usar más temas.
                    ((Control.FlatDateTime)c).BackColor = Color.FromArgb(35, 45, 54);
                    ((Control.FlatDateTime)c).lineMainColor = Color.FromArgb(10, 140, 129);
                    ((Control.FlatDateTime)c).ColorLine = Color.FromArgb(10, 140, 129);
                    ((Control.FlatDateTime)c).ColorText = Color.WhiteSmoke;
                    ((Control.FlatDateTime)c).ColorFocus = Color.MintCream;
                    ((Control.FlatDateTime)c).ColorTitle = Color.Gainsboro;
                }
            }
        }
        private void SetGridColor(Color backColor, Color headBackColor, Color headFontColor
            ,Color bodyFontColor)
        {
            // Solo en el panel de nombre pnlGrid se insertará la Grilla... casi contrario no se reflejará efectos.
            foreach (System.Windows.Forms.Control c in pnlGrid.Controls)
            {
                if (c is Autonomo.Control.CustomGrid)
                {
                    ((Control.CustomGrid)c).BackgroundColor = backColor;                    
                    ((Control.CustomGrid)c).HeaderColor = headBackColor;
                    ((Control.CustomGrid)c).HeaderForeColor = headFontColor;
                    ((Control.CustomGrid)c).BodyForeColor = bodyFontColor;
                    ((Control.CustomGrid)c).CellStyleBackColor = backColor;
                }
            }
        }

        private void SetPanelBackAndTextColor(Color panelColor, Color titleForeColor, Color detailForeColor)
        {
            pnlContenedor.BackColor = panelColor;
            Title.ForeColor = titleForeColor;
            DescriptionData.ForeColor = detailForeColor;           

        }
        private void SetButtonPanelAndTextColor(Color panelColor, Color foreColor, Theme theme)
        {
            pnlTopButton.BackColor = panelColor;
            TopLine.BackColor = panelColor;
            BottomLine.BackColor = panelColor;

            btnNuevo.ForeColor = foreColor;
            btnEditar.ForeColor = foreColor;
            btnEliminar.ForeColor = foreColor;
            btnExportar.ForeColor = foreColor;
            btnCommand1.ForeColor = foreColor;
            btnCommand2.ForeColor = foreColor; 
            btnCommand3.ForeColor = foreColor;

            // Validar si los temas son oscuros o claros.
            if (theme == Theme.Dark)// Si el fondo del panel es oscuro:
            {
                btnNuevo.Image = Properties.Resources.Add20;
                btnEditar.Image = Properties.Resources.Edit20;
                btnEliminar.Image = Properties.Resources.Delete20;
                btnExportar.Image = Properties.Resources.Excel20;
            }
        }
        #endregion

        public void SetLineColor(Color color)
        {
            TopLine.BackColor = color;
            BottomLine.BackColor = color;
        }

        private void Registry_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\r' || e.KeyChar == (char)Keys.Escape)
                e.Handled = true;
        }
    }
}
