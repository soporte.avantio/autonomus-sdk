﻿namespace Autonomo.Object
{
    partial class MenuLeftPanel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MenuLeftPanel));
            this.Contenedor = new System.Windows.Forms.Panel();
            this.SubContenedor = new System.Windows.Forms.Panel();
            this.Izquierda = new System.Windows.Forms.Label();
            this.Derecha = new System.Windows.Forms.Label();
            this.Abajo = new System.Windows.Forms.Label();
            this.Arriba = new System.Windows.Forms.Label();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.PanelModulo = new System.Windows.Forms.Panel();
            this.PanelConfiguracion = new System.Windows.Forms.Panel();
            this.Configuracion = new Autonomo.Control.LabelModule();
            this.PanelReporte = new System.Windows.Forms.Panel();
            this.Reporte = new Autonomo.Control.LabelModule();
            this.PanelAlmacen = new System.Windows.Forms.Panel();
            this.Almacen = new Autonomo.Control.LabelModule();
            this.PanelProveedor = new System.Windows.Forms.Panel();
            this.Proveedor = new Autonomo.Control.LabelModule();
            this.PanelCompra = new System.Windows.Forms.Panel();
            this.Compra = new Autonomo.Control.LabelModule();
            this.PanelMovimiento = new System.Windows.Forms.Panel();
            this.Movimiento = new Autonomo.Control.LabelModule();
            this.PanelCliente = new System.Windows.Forms.Panel();
            this.Cliente = new Autonomo.Control.LabelModule();
            this.PanelVenta = new System.Windows.Forms.Panel();
            this.Venta = new Autonomo.Control.LabelModule();
            this.PanelCaja = new System.Windows.Forms.Panel();
            this.Caja = new Autonomo.Control.LabelModule();
            this.PanelInfo = new System.Windows.Forms.Panel();
            this.UserDisplayText = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SubTitulo = new System.Windows.Forms.Label();
            this.EmpresaNombre = new System.Windows.Forms.Label();
            this.Logo = new System.Windows.Forms.PictureBox();
            this.PanelControl = new System.Windows.Forms.Panel();
            this.Detalle = new System.Windows.Forms.Label();
            this.Bell = new System.Windows.Forms.Button();
            this.Software = new System.Windows.Forms.Button();
            this.Ayuda = new System.Windows.Forms.Button();
            this.Minimizar = new System.Windows.Forms.Button();
            this.Salir = new System.Windows.Forms.Button();
            this.tmrDateTime = new System.Windows.Forms.Timer(this.components);
            this.Contenedor.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            this.PanelModulo.SuspendLayout();
            this.PanelConfiguracion.SuspendLayout();
            this.PanelReporte.SuspendLayout();
            this.PanelAlmacen.SuspendLayout();
            this.PanelProveedor.SuspendLayout();
            this.PanelCompra.SuspendLayout();
            this.PanelMovimiento.SuspendLayout();
            this.PanelCliente.SuspendLayout();
            this.PanelVenta.SuspendLayout();
            this.PanelCaja.SuspendLayout();
            this.PanelInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.PanelControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Controls.Add(this.SubContenedor);
            this.Contenedor.Controls.Add(this.Izquierda);
            this.Contenedor.Controls.Add(this.Derecha);
            this.Contenedor.Controls.Add(this.Abajo);
            this.Contenedor.Controls.Add(this.Arriba);
            this.Contenedor.Controls.Add(this.PanelMenu);
            this.Contenedor.Controls.Add(this.PanelControl);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 0);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(960, 753);
            this.Contenedor.TabIndex = 0;
            // 
            // SubContenedor
            // 
            this.SubContenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SubContenedor.Location = new System.Drawing.Point(249, 56);
            this.SubContenedor.Name = "SubContenedor";
            this.SubContenedor.Size = new System.Drawing.Size(691, 687);
            this.SubContenedor.TabIndex = 2;
            // 
            // Izquierda
            // 
            this.Izquierda.Dock = System.Windows.Forms.DockStyle.Right;
            this.Izquierda.Location = new System.Drawing.Point(940, 56);
            this.Izquierda.Name = "Izquierda";
            this.Izquierda.Size = new System.Drawing.Size(20, 687);
            this.Izquierda.TabIndex = 6;
            // 
            // Derecha
            // 
            this.Derecha.Dock = System.Windows.Forms.DockStyle.Left;
            this.Derecha.Location = new System.Drawing.Point(229, 56);
            this.Derecha.Name = "Derecha";
            this.Derecha.Size = new System.Drawing.Size(20, 687);
            this.Derecha.TabIndex = 5;
            // 
            // Abajo
            // 
            this.Abajo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Abajo.Location = new System.Drawing.Point(229, 743);
            this.Abajo.Name = "Abajo";
            this.Abajo.Size = new System.Drawing.Size(731, 10);
            this.Abajo.TabIndex = 4;
            // 
            // Arriba
            // 
            this.Arriba.Dock = System.Windows.Forms.DockStyle.Top;
            this.Arriba.Location = new System.Drawing.Point(229, 36);
            this.Arriba.Name = "Arriba";
            this.Arriba.Size = new System.Drawing.Size(731, 20);
            this.Arriba.TabIndex = 3;
            // 
            // PanelMenu
            // 
            this.PanelMenu.Controls.Add(this.PanelModulo);
            this.PanelMenu.Controls.Add(this.PanelInfo);
            this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.PanelMenu.Location = new System.Drawing.Point(0, 36);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(229, 717);
            this.PanelMenu.TabIndex = 1;
            // 
            // PanelModulo
            // 
            this.PanelModulo.AutoScroll = true;
            this.PanelModulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(252)))));
            this.PanelModulo.Controls.Add(this.PanelConfiguracion);
            this.PanelModulo.Controls.Add(this.PanelReporte);
            this.PanelModulo.Controls.Add(this.PanelAlmacen);
            this.PanelModulo.Controls.Add(this.PanelProveedor);
            this.PanelModulo.Controls.Add(this.PanelCompra);
            this.PanelModulo.Controls.Add(this.PanelMovimiento);
            this.PanelModulo.Controls.Add(this.PanelCliente);
            this.PanelModulo.Controls.Add(this.PanelVenta);
            this.PanelModulo.Controls.Add(this.PanelCaja);
            this.PanelModulo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelModulo.Location = new System.Drawing.Point(0, 142);
            this.PanelModulo.Name = "PanelModulo";
            this.PanelModulo.Size = new System.Drawing.Size(229, 575);
            this.PanelModulo.TabIndex = 0;
            // 
            // PanelConfiguracion
            // 
            this.PanelConfiguracion.Controls.Add(this.Configuracion);
            this.PanelConfiguracion.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelConfiguracion.Location = new System.Drawing.Point(0, 416);
            this.PanelConfiguracion.Name = "PanelConfiguracion";
            this.PanelConfiguracion.Size = new System.Drawing.Size(229, 52);
            this.PanelConfiguracion.TabIndex = 7;
            // 
            // Configuracion
            // 
            this.Configuracion.BackColor = System.Drawing.Color.Transparent;
            this.Configuracion.ColorDetailText = System.Drawing.Color.Gray;
            this.Configuracion.ColorText = System.Drawing.SystemColors.ControlText;
            this.Configuracion.DetailText = "Administrar parámetros";
            this.Configuracion.Dock = System.Windows.Forms.DockStyle.Top;
            this.Configuracion.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Configuracion.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Configuracion.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Configuracion.ImageDown = ((System.Drawing.Image)(resources.GetObject("Configuracion.ImageDown")));
            this.Configuracion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Configuracion.ImageIcon")));
            this.Configuracion.Location = new System.Drawing.Point(0, 0);
            this.Configuracion.Name = "Configuracion";
            this.Configuracion.Size = new System.Drawing.Size(229, 44);
            this.Configuracion.TabIndex = 4;
            this.Configuracion.Text = "Configuraciones";
            // 
            // PanelReporte
            // 
            this.PanelReporte.Controls.Add(this.Reporte);
            this.PanelReporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelReporte.Location = new System.Drawing.Point(0, 364);
            this.PanelReporte.Name = "PanelReporte";
            this.PanelReporte.Size = new System.Drawing.Size(229, 52);
            this.PanelReporte.TabIndex = 6;
            // 
            // Reporte
            // 
            this.Reporte.BackColor = System.Drawing.Color.Transparent;
            this.Reporte.ColorDetailText = System.Drawing.Color.Gray;
            this.Reporte.ColorText = System.Drawing.SystemColors.ControlText;
            this.Reporte.DetailText = "Resumen de estadístico";
            this.Reporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.Reporte.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reporte.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reporte.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Reporte.ImageDown = ((System.Drawing.Image)(resources.GetObject("Reporte.ImageDown")));
            this.Reporte.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Reporte.ImageIcon")));
            this.Reporte.Location = new System.Drawing.Point(0, 0);
            this.Reporte.Name = "Reporte";
            this.Reporte.Size = new System.Drawing.Size(229, 44);
            this.Reporte.TabIndex = 4;
            this.Reporte.Text = "Reportes";
            // 
            // PanelAlmacen
            // 
            this.PanelAlmacen.Controls.Add(this.Almacen);
            this.PanelAlmacen.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelAlmacen.Location = new System.Drawing.Point(0, 312);
            this.PanelAlmacen.Name = "PanelAlmacen";
            this.PanelAlmacen.Size = new System.Drawing.Size(229, 52);
            this.PanelAlmacen.TabIndex = 5;
            // 
            // Almacen
            // 
            this.Almacen.BackColor = System.Drawing.Color.Transparent;
            this.Almacen.ColorDetailText = System.Drawing.Color.Gray;
            this.Almacen.ColorText = System.Drawing.SystemColors.ControlText;
            this.Almacen.DetailText = "Administrar Maestros";
            this.Almacen.Dock = System.Windows.Forms.DockStyle.Top;
            this.Almacen.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Almacen.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Almacen.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Almacen.ImageDown = ((System.Drawing.Image)(resources.GetObject("Almacen.ImageDown")));
            this.Almacen.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Almacen.ImageIcon")));
            this.Almacen.Location = new System.Drawing.Point(0, 0);
            this.Almacen.Name = "Almacen";
            this.Almacen.Size = new System.Drawing.Size(229, 44);
            this.Almacen.TabIndex = 4;
            this.Almacen.Text = "Almacén && Artículos";
            // 
            // PanelProveedor
            // 
            this.PanelProveedor.Controls.Add(this.Proveedor);
            this.PanelProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelProveedor.Location = new System.Drawing.Point(0, 260);
            this.PanelProveedor.Name = "PanelProveedor";
            this.PanelProveedor.Size = new System.Drawing.Size(229, 52);
            this.PanelProveedor.TabIndex = 4;
            // 
            // Proveedor
            // 
            this.Proveedor.BackColor = System.Drawing.Color.Transparent;
            this.Proveedor.ColorDetailText = System.Drawing.Color.Gray;
            this.Proveedor.ColorText = System.Drawing.SystemColors.ControlText;
            this.Proveedor.DetailText = "Administrar Proveedor";
            this.Proveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.Proveedor.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Proveedor.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Proveedor.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Proveedor.ImageDown = ((System.Drawing.Image)(resources.GetObject("Proveedor.ImageDown")));
            this.Proveedor.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Proveedor.ImageIcon")));
            this.Proveedor.Location = new System.Drawing.Point(0, 0);
            this.Proveedor.Name = "Proveedor";
            this.Proveedor.Size = new System.Drawing.Size(229, 44);
            this.Proveedor.TabIndex = 4;
            this.Proveedor.Text = "Proveedor";
            // 
            // PanelCompra
            // 
            this.PanelCompra.Controls.Add(this.Compra);
            this.PanelCompra.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelCompra.Location = new System.Drawing.Point(0, 208);
            this.PanelCompra.Name = "PanelCompra";
            this.PanelCompra.Size = new System.Drawing.Size(229, 52);
            this.PanelCompra.TabIndex = 3;
            // 
            // Compra
            // 
            this.Compra.BackColor = System.Drawing.Color.Transparent;
            this.Compra.ColorDetailText = System.Drawing.Color.Gray;
            this.Compra.ColorText = System.Drawing.SystemColors.ControlText;
            this.Compra.DetailText = "Administrar Compras";
            this.Compra.Dock = System.Windows.Forms.DockStyle.Top;
            this.Compra.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Compra.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Compra.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Compra.ImageDown = ((System.Drawing.Image)(resources.GetObject("Compra.ImageDown")));
            this.Compra.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Compra.ImageIcon")));
            this.Compra.Location = new System.Drawing.Point(0, 0);
            this.Compra.Name = "Compra";
            this.Compra.Size = new System.Drawing.Size(229, 44);
            this.Compra.TabIndex = 4;
            this.Compra.Text = "Compras";
            // 
            // PanelMovimiento
            // 
            this.PanelMovimiento.Controls.Add(this.Movimiento);
            this.PanelMovimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelMovimiento.Location = new System.Drawing.Point(0, 156);
            this.PanelMovimiento.Name = "PanelMovimiento";
            this.PanelMovimiento.Size = new System.Drawing.Size(229, 52);
            this.PanelMovimiento.TabIndex = 8;
            // 
            // Movimiento
            // 
            this.Movimiento.BackColor = System.Drawing.Color.Transparent;
            this.Movimiento.ColorDetailText = System.Drawing.Color.Gray;
            this.Movimiento.ColorText = System.Drawing.SystemColors.ControlText;
            this.Movimiento.DetailText = "Administrar Movimientos";
            this.Movimiento.Dock = System.Windows.Forms.DockStyle.Top;
            this.Movimiento.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Movimiento.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Movimiento.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Movimiento.ImageDown = ((System.Drawing.Image)(resources.GetObject("Movimiento.ImageDown")));
            this.Movimiento.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Movimiento.ImageIcon")));
            this.Movimiento.Location = new System.Drawing.Point(0, 0);
            this.Movimiento.Name = "Movimiento";
            this.Movimiento.Size = new System.Drawing.Size(229, 44);
            this.Movimiento.TabIndex = 4;
            this.Movimiento.Text = "Movimientos";
            // 
            // PanelCliente
            // 
            this.PanelCliente.Controls.Add(this.Cliente);
            this.PanelCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelCliente.Location = new System.Drawing.Point(0, 104);
            this.PanelCliente.Name = "PanelCliente";
            this.PanelCliente.Size = new System.Drawing.Size(229, 52);
            this.PanelCliente.TabIndex = 2;
            // 
            // Cliente
            // 
            this.Cliente.BackColor = System.Drawing.Color.Transparent;
            this.Cliente.ColorDetailText = System.Drawing.Color.Gray;
            this.Cliente.ColorText = System.Drawing.SystemColors.ControlText;
            this.Cliente.DetailText = "Administrar Clientes";
            this.Cliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.Cliente.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cliente.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cliente.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cliente.ImageDown = ((System.Drawing.Image)(resources.GetObject("Cliente.ImageDown")));
            this.Cliente.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Cliente.ImageIcon")));
            this.Cliente.Location = new System.Drawing.Point(0, 0);
            this.Cliente.Name = "Cliente";
            this.Cliente.Size = new System.Drawing.Size(229, 44);
            this.Cliente.TabIndex = 4;
            this.Cliente.Text = "Clientes";
            // 
            // PanelVenta
            // 
            this.PanelVenta.Controls.Add(this.Venta);
            this.PanelVenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelVenta.Location = new System.Drawing.Point(0, 52);
            this.PanelVenta.Name = "PanelVenta";
            this.PanelVenta.Size = new System.Drawing.Size(229, 52);
            this.PanelVenta.TabIndex = 1;
            // 
            // Venta
            // 
            this.Venta.BackColor = System.Drawing.Color.Transparent;
            this.Venta.ColorDetailText = System.Drawing.Color.Gray;
            this.Venta.ColorText = System.Drawing.SystemColors.ControlText;
            this.Venta.DetailText = "Administrar Ventas";
            this.Venta.Dock = System.Windows.Forms.DockStyle.Top;
            this.Venta.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Venta.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Venta.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Venta.ImageDown = ((System.Drawing.Image)(resources.GetObject("Venta.ImageDown")));
            this.Venta.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Venta.ImageIcon")));
            this.Venta.Location = new System.Drawing.Point(0, 0);
            this.Venta.Name = "Venta";
            this.Venta.Size = new System.Drawing.Size(229, 44);
            this.Venta.TabIndex = 4;
            this.Venta.Text = "Ventas";
            // 
            // PanelCaja
            // 
            this.PanelCaja.Controls.Add(this.Caja);
            this.PanelCaja.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelCaja.Location = new System.Drawing.Point(0, 0);
            this.PanelCaja.Name = "PanelCaja";
            this.PanelCaja.Size = new System.Drawing.Size(229, 52);
            this.PanelCaja.TabIndex = 0;
            // 
            // Caja
            // 
            this.Caja.BackColor = System.Drawing.Color.Transparent;
            this.Caja.ColorDetailText = System.Drawing.Color.Gray;
            this.Caja.ColorText = System.Drawing.SystemColors.ControlText;
            this.Caja.DetailText = "Administrador de Caja";
            this.Caja.Dock = System.Windows.Forms.DockStyle.Top;
            this.Caja.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Caja.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Caja.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Caja.ImageDown = ((System.Drawing.Image)(resources.GetObject("Caja.ImageDown")));
            this.Caja.ImageIcon = ((System.Drawing.Image)(resources.GetObject("Caja.ImageIcon")));
            this.Caja.Location = new System.Drawing.Point(0, 0);
            this.Caja.Name = "Caja";
            this.Caja.Size = new System.Drawing.Size(229, 44);
            this.Caja.TabIndex = 3;
            this.Caja.Text = "Caja";
            // 
            // PanelInfo
            // 
            this.PanelInfo.Controls.Add(this.UserDisplayText);
            this.PanelInfo.Controls.Add(this.label2);
            this.PanelInfo.Controls.Add(this.SubTitulo);
            this.PanelInfo.Controls.Add(this.EmpresaNombre);
            this.PanelInfo.Controls.Add(this.Logo);
            this.PanelInfo.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelInfo.Location = new System.Drawing.Point(0, 0);
            this.PanelInfo.Name = "PanelInfo";
            this.PanelInfo.Size = new System.Drawing.Size(229, 142);
            this.PanelInfo.TabIndex = 1;
            // 
            // UserDisplayText
            // 
            this.UserDisplayText.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.UserDisplayText.Font = new System.Drawing.Font("Verdana", 10F);
            this.UserDisplayText.ForeColor = System.Drawing.Color.Black;
            this.UserDisplayText.Location = new System.Drawing.Point(0, 94);
            this.UserDisplayText.Name = "UserDisplayText";
            this.UserDisplayText.Size = new System.Drawing.Size(229, 40);
            this.UserDisplayText.TabIndex = 5;
            this.UserDisplayText.Text = "Bienvenido Usuario";
            this.UserDisplayText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(0, 134);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(229, 8);
            this.label2.TabIndex = 6;
            this.label2.Text = "- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - ";
            // 
            // SubTitulo
            // 
            this.SubTitulo.AutoSize = true;
            this.SubTitulo.Font = new System.Drawing.Font("Verdana", 8F);
            this.SubTitulo.ForeColor = System.Drawing.Color.Black;
            this.SubTitulo.Location = new System.Drawing.Point(25, 71);
            this.SubTitulo.Name = "SubTitulo";
            this.SubTitulo.Size = new System.Drawing.Size(178, 13);
            this.SubTitulo.TabIndex = 4;
            this.SubTitulo.Text = "Sistema de Gestión de Ventas";
            // 
            // EmpresaNombre
            // 
            this.EmpresaNombre.AutoSize = true;
            this.EmpresaNombre.Font = new System.Drawing.Font("Verdana", 13F);
            this.EmpresaNombre.ForeColor = System.Drawing.Color.Black;
            this.EmpresaNombre.Location = new System.Drawing.Point(19, 50);
            this.EmpresaNombre.Name = "EmpresaNombre";
            this.EmpresaNombre.Size = new System.Drawing.Size(190, 22);
            this.EmpresaNombre.TabIndex = 3;
            this.EmpresaNombre.Text = "Empresa Deportiva ";
            // 
            // Logo
            // 
            this.Logo.Image = ((System.Drawing.Image)(resources.GetObject("Logo.Image")));
            this.Logo.Location = new System.Drawing.Point(85, 6);
            this.Logo.Name = "Logo";
            this.Logo.Size = new System.Drawing.Size(59, 41);
            this.Logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Logo.TabIndex = 1;
            this.Logo.TabStop = false;
            // 
            // PanelControl
            // 
            this.PanelControl.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(232)))), ((int)(((byte)(232)))));
            this.PanelControl.Controls.Add(this.Detalle);
            this.PanelControl.Controls.Add(this.Bell);
            this.PanelControl.Controls.Add(this.Software);
            this.PanelControl.Controls.Add(this.Ayuda);
            this.PanelControl.Controls.Add(this.Minimizar);
            this.PanelControl.Controls.Add(this.Salir);
            this.PanelControl.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelControl.Location = new System.Drawing.Point(0, 0);
            this.PanelControl.Name = "PanelControl";
            this.PanelControl.Size = new System.Drawing.Size(960, 36);
            this.PanelControl.TabIndex = 0;
            // 
            // Detalle
            // 
            this.Detalle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Detalle.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Detalle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Detalle.Location = new System.Drawing.Point(0, 0);
            this.Detalle.Name = "Detalle";
            this.Detalle.Size = new System.Drawing.Size(730, 36);
            this.Detalle.TabIndex = 11;
            this.Detalle.Text = "Modulo Ventas  :  Conexión 192.168.1.200  SportStore  :  Tipo de cambio sunat 3.5" +
    "6  :  I.G.V. 0.18";
            this.Detalle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Bell
            // 
            this.Bell.Dock = System.Windows.Forms.DockStyle.Right;
            this.Bell.FlatAppearance.BorderSize = 0;
            this.Bell.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Bell.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Bell.ForeColor = System.Drawing.Color.White;
            this.Bell.Image = ((System.Drawing.Image)(resources.GetObject("Bell.Image")));
            this.Bell.Location = new System.Drawing.Point(730, 0);
            this.Bell.Name = "Bell";
            this.Bell.Size = new System.Drawing.Size(46, 36);
            this.Bell.TabIndex = 10;
            this.Bell.Text = "0";
            this.Bell.UseVisualStyleBackColor = true;
            // 
            // Software
            // 
            this.Software.Dock = System.Windows.Forms.DockStyle.Right;
            this.Software.FlatAppearance.BorderSize = 0;
            this.Software.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Software.Font = new System.Drawing.Font("Verdana", 10F);
            this.Software.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Software.Location = new System.Drawing.Point(776, 0);
            this.Software.Name = "Software";
            this.Software.Size = new System.Drawing.Size(46, 36);
            this.Software.TabIndex = 9;
            this.Software.Text = "s";
            this.Software.UseVisualStyleBackColor = true;
            // 
            // Ayuda
            // 
            this.Ayuda.Dock = System.Windows.Forms.DockStyle.Right;
            this.Ayuda.FlatAppearance.BorderSize = 0;
            this.Ayuda.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Ayuda.Font = new System.Drawing.Font("Verdana", 10F);
            this.Ayuda.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Ayuda.Location = new System.Drawing.Point(822, 0);
            this.Ayuda.Name = "Ayuda";
            this.Ayuda.Size = new System.Drawing.Size(46, 36);
            this.Ayuda.TabIndex = 8;
            this.Ayuda.Text = "?";
            this.Ayuda.UseVisualStyleBackColor = true;
            // 
            // Minimizar
            // 
            this.Minimizar.Dock = System.Windows.Forms.DockStyle.Right;
            this.Minimizar.FlatAppearance.BorderSize = 0;
            this.Minimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Minimizar.Font = new System.Drawing.Font("Verdana", 10F);
            this.Minimizar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Minimizar.Location = new System.Drawing.Point(868, 0);
            this.Minimizar.Name = "Minimizar";
            this.Minimizar.Size = new System.Drawing.Size(46, 36);
            this.Minimizar.TabIndex = 6;
            this.Minimizar.Text = "─";
            this.Minimizar.UseVisualStyleBackColor = true;
            this.Minimizar.Click += new System.EventHandler(this.Miminizar_Click);
            // 
            // Salir
            // 
            this.Salir.Dock = System.Windows.Forms.DockStyle.Right;
            this.Salir.FlatAppearance.BorderSize = 0;
            this.Salir.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Red;
            this.Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Salir.Font = new System.Drawing.Font("Verdana", 10F);
            this.Salir.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Salir.Location = new System.Drawing.Point(914, 0);
            this.Salir.Name = "Salir";
            this.Salir.Size = new System.Drawing.Size(46, 36);
            this.Salir.TabIndex = 7;
            this.Salir.Text = "X";
            this.Salir.UseVisualStyleBackColor = true;
            this.Salir.Click += new System.EventHandler(this.Salir_Click);
            // 
            // tmrDateTime
            // 
            this.tmrDateTime.Enabled = true;
            this.tmrDateTime.Interval = 10;
            this.tmrDateTime.Tick += new System.EventHandler(this.tmrDateTime_Tick);
            // 
            // MenuLeftPanel
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(249)))), ((int)(((byte)(252)))));
            this.ClientSize = new System.Drawing.Size(960, 753);
            this.Controls.Add(this.Contenedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "MenuLeftPanel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MenuLeftPanel";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MenuLeftPanel_FormClosing);
            this.Load += new System.EventHandler(this.MenuLeftPanel_Load);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.MenuLeftPanel_KeyPress);
            this.Contenedor.ResumeLayout(false);
            this.PanelMenu.ResumeLayout(false);
            this.PanelModulo.ResumeLayout(false);
            this.PanelConfiguracion.ResumeLayout(false);
            this.PanelReporte.ResumeLayout(false);
            this.PanelAlmacen.ResumeLayout(false);
            this.PanelProveedor.ResumeLayout(false);
            this.PanelCompra.ResumeLayout(false);
            this.PanelMovimiento.ResumeLayout(false);
            this.PanelCliente.ResumeLayout(false);
            this.PanelVenta.ResumeLayout(false);
            this.PanelCaja.ResumeLayout(false);
            this.PanelInfo.ResumeLayout(false);
            this.PanelInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.PanelControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.Panel PanelMenu;
        private System.Windows.Forms.Panel PanelModulo;
        private System.Windows.Forms.Panel PanelInfo;
        private System.Windows.Forms.Panel PanelControl;
        private System.Windows.Forms.Label Arriba;
        private System.Windows.Forms.Label Izquierda;
        private System.Windows.Forms.Label Derecha;
        private System.Windows.Forms.Label Abajo;
        private System.Windows.Forms.Button Bell;
        private System.Windows.Forms.Button Software;
        private System.Windows.Forms.Button Ayuda;
        private System.Windows.Forms.Button Minimizar;
        private System.Windows.Forms.Button Salir;
        private System.Windows.Forms.Label Detalle;
        protected System.Windows.Forms.Label UserDisplayText;
        private System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label SubTitulo;
        public System.Windows.Forms.Label EmpresaNombre;
        public System.Windows.Forms.PictureBox Logo;
        private System.Windows.Forms.Timer tmrDateTime;
        public System.Windows.Forms.Panel PanelVenta;
        public System.Windows.Forms.Panel PanelConfiguracion;
        public System.Windows.Forms.Panel PanelCompra;
        public System.Windows.Forms.Panel PanelAlmacen;
        public System.Windows.Forms.Panel PanelReporte;
        public System.Windows.Forms.Panel PanelCliente;
        public System.Windows.Forms.Panel PanelProveedor;
        public System.Windows.Forms.Panel PanelCaja;
        private Autonomo.Control.LabelModule Venta;
        private Autonomo.Control.LabelModule Caja;
        private Autonomo.Control.LabelModule Configuracion;
        private Autonomo.Control.LabelModule Compra;
        private Autonomo.Control.LabelModule Almacen;
        private Autonomo.Control.LabelModule Reporte;
        private Autonomo.Control.LabelModule Cliente;
        private Autonomo.Control.LabelModule Proveedor;
        public System.Windows.Forms.Panel SubContenedor;
        public System.Windows.Forms.Panel PanelMovimiento;
        private Control.LabelModule Movimiento;
    }
}