﻿namespace Autonomo.Object
{
    partial class Query
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Query));
            this.Contenedor = new System.Windows.Forms.Panel();
            this.Body = new System.Windows.Forms.Panel();
            this.PanelToGrid = new System.Windows.Forms.Panel();
            this.grdData = new Autonomo.Control.CustomGrid();
            this.PanelToFind = new System.Windows.Forms.Panel();
            this.txConsulta = new Autonomo.Control.FlatFindText();
            this.Footer = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.Header = new System.Windows.Forms.Panel();
            this.Title = new System.Windows.Forms.Label();
            this.cmdClose = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.PanelToGrid.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).BeginInit();
            this.PanelToFind.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Controls.Add(this.Body);
            this.Contenedor.Controls.Add(this.Footer);
            this.Contenedor.Controls.Add(this.Header);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 0);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(362, 425);
            this.Contenedor.TabIndex = 1;
            // 
            // Body
            // 
            this.Body.AutoScroll = true;
            this.Body.Controls.Add(this.PanelToGrid);
            this.Body.Controls.Add(this.PanelToFind);
            this.Body.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Body.Location = new System.Drawing.Point(0, 41);
            this.Body.Name = "Body";
            this.Body.Size = new System.Drawing.Size(362, 353);
            this.Body.TabIndex = 1;
            // 
            // PanelToGrid
            // 
            this.PanelToGrid.Controls.Add(this.grdData);
            this.PanelToGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelToGrid.Location = new System.Drawing.Point(0, 57);
            this.PanelToGrid.Name = "PanelToGrid";
            this.PanelToGrid.Size = new System.Drawing.Size(362, 296);
            this.PanelToGrid.TabIndex = 1;
            // 
            // grdData
            // 
            this.grdData.AllowUserToAddRows = false;
            this.grdData.AllowUserToDeleteRows = false;
            this.grdData.AllowUserToResizeRows = false;
            this.grdData.BackgroundColor = System.Drawing.Color.White;
            this.grdData.BodyFont = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdData.BodyForeColor = System.Drawing.SystemColors.ControlText;
            this.grdData.BodySelectColor = System.Drawing.SystemColors.Highlight;
            this.grdData.BodySelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.grdData.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdData.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdData.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdData.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.grdData.ColumnHeadersHeight = 34;
            this.grdData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdData.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdData.EnableHeadersVisualStyles = false;
            this.grdData.HeaderColor = System.Drawing.SystemColors.Control;
            this.grdData.HeaderFont = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdData.HeaderForeColor = System.Drawing.SystemColors.WindowText;
            this.grdData.Location = new System.Drawing.Point(0, 0);
            this.grdData.MultiSelect = false;
            this.grdData.Name = "grdData";
            this.grdData.RowHeadersVisible = false;
            this.grdData.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdData.Size = new System.Drawing.Size(362, 296);
            this.grdData.TabIndex = 0;
            this.grdData.TabStop = false;
            this.grdData.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdData_CellDoubleClick);
            // 
            // PanelToFind
            // 
            this.PanelToFind.Controls.Add(this.txConsulta);
            this.PanelToFind.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelToFind.Location = new System.Drawing.Point(0, 0);
            this.PanelToFind.Name = "PanelToFind";
            this.PanelToFind.Size = new System.Drawing.Size(362, 57);
            this.PanelToFind.TabIndex = 1;
            // 
            // txConsulta
            // 
            this.txConsulta.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txConsulta.BackColor = System.Drawing.Color.White;
            this.txConsulta.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txConsulta.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txConsulta.ColorLine = System.Drawing.Color.LightGray;
            this.txConsulta.ColorText = System.Drawing.SystemColors.WindowText;
            this.txConsulta.ColorTitle = System.Drawing.Color.Gray;
            this.txConsulta.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txConsulta.DockIcon = System.Windows.Forms.DockStyle.Right;
            this.txConsulta.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txConsulta.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txConsulta.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txConsulta.ImageIcon")));
            this.txConsulta.Location = new System.Drawing.Point(0, 13);
            this.txConsulta.MaterialStyle = false;
            this.txConsulta.MaxLength = 32767;
            this.txConsulta.MultiLineText = false;
            this.txConsulta.Name = "txConsulta";
            this.txConsulta.PasswordChar = '\0';
            this.txConsulta.Placeholder = "";
            this.txConsulta.ReadOnly = false;
            this.txConsulta.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txConsulta.Size = new System.Drawing.Size(362, 44);
            this.txConsulta.SizeLine = 2;
            this.txConsulta.TabIndex = 0;
            this.txConsulta.TextId = "";
            this.txConsulta.Title = "Press enter to filter data...";
            this.txConsulta.VisibleIcon = true;
            this.txConsulta.VisibleTitle = true;
            // 
            // Footer
            // 
            this.Footer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.Footer.Controls.Add(this.label1);
            this.Footer.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Footer.Location = new System.Drawing.Point(0, 394);
            this.Footer.Name = "Footer";
            this.Footer.Size = new System.Drawing.Size(362, 31);
            this.Footer.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(362, 31);
            this.label1.TabIndex = 0;
            this.label1.Text = "Double Click to select data";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Header
            // 
            this.Header.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.Header.Controls.Add(this.Title);
            this.Header.Controls.Add(this.cmdClose);
            this.Header.Dock = System.Windows.Forms.DockStyle.Top;
            this.Header.Location = new System.Drawing.Point(0, 0);
            this.Header.Name = "Header";
            this.Header.Size = new System.Drawing.Size(362, 41);
            this.Header.TabIndex = 3;
            // 
            // Title
            // 
            this.Title.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Title.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Title.Location = new System.Drawing.Point(0, 0);
            this.Title.Name = "Title";
            this.Title.Size = new System.Drawing.Size(321, 41);
            this.Title.TabIndex = 90;
            this.Title.Text = "Formulary Name";
            this.Title.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Title.MouseDown += new System.Windows.Forms.MouseEventHandler(this.Title_MouseDown);
            // 
            // cmdClose
            // 
            this.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClose.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdClose.Image = ((System.Drawing.Image)(resources.GetObject("cmdClose.Image")));
            this.cmdClose.Location = new System.Drawing.Point(321, 0);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(41, 41);
            this.cmdClose.TabIndex = 89;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // Query
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(362, 425);
            this.Controls.Add(this.Contenedor);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Name = "Query";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Query";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Query_KeyDown);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Query_KeyPress);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.PanelToGrid.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdData)).EndInit();
            this.PanelToFind.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Panel Contenedor;
        public System.Windows.Forms.Panel Body;
        public System.Windows.Forms.Panel Footer;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Panel Header;
        public System.Windows.Forms.Label Title;
        private System.Windows.Forms.Label cmdClose;
        private System.Windows.Forms.Panel PanelToGrid;
        private System.Windows.Forms.Panel PanelToFind;
        public Control.CustomGrid grdData;
        public Control.FlatFindText txConsulta;
    }
}