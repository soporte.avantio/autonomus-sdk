﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Class
{
    public class Fomulary
    {
        public static string ShowModal(Form fModal, string tag)
        {
            var bg = new Form();
        
            using (fModal)
            {
                bg.StartPosition = FormStartPosition.Manual;
                bg.FormBorderStyle = FormBorderStyle.None;
                bg.Opacity = 0.7d;
                bg.BackColor = Color.Black;
                bg.WindowState = FormWindowState.Maximized;
                bg.TopMost = true;
                bg.Location = fModal.Location;
                bg.ShowInTaskbar = false;
                bg.Show();
                fModal.TopMost = true;
                fModal.Tag = tag;
                fModal.Owner = bg;
                fModal.ShowDialog();//
                // Continua cuando se cierra fModal
                bg.Dispose();
                return fModal.Tag.ToString();
            }
        }
        public static string ShowModal(Form fModal, string tag, bool topMost)
        {
            var bg = new Form();

            using (fModal)
            {
                bg.StartPosition = FormStartPosition.Manual;
                bg.FormBorderStyle = FormBorderStyle.None;
                bg.Opacity = 0.7d;
                bg.BackColor = Color.Black;
                bg.WindowState = FormWindowState.Maximized;
                bg.TopMost = topMost;
                bg.Location = fModal.Location;
                bg.ShowInTaskbar = false;
                bg.Show();
                fModal.TopMost = topMost;
                fModal.Tag = tag;
                fModal.Owner = bg;
                fModal.ShowDialog();//
                // Continua cuando se cierra fModal
                bg.Dispose();
                return fModal.Tag.ToString();
            }
        }

        public static void ShowFormInPanel(Form fHijo, Panel panel)
        {
            bool isOpen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Name == fHijo.Name)
                {
                    isOpen = true;
                    f.BringToFront();
                    break;
                }
            }
            if (isOpen==false)
            {
                fHijo.TopLevel = false;
                fHijo.Dock = DockStyle.Fill;
                panel.Controls.Add(fHijo);
                fHijo.Show();
                fHijo.BringToFront();
            }
        }
        public static void ShowNewFormInPanel(Form fHijo, Form fPapa, Panel pnl)
        {
            bool isopen = false;
            foreach (Form f in Application.OpenForms)
            {
                if (f.Name == fPapa.Name)
                {
                    if (f.Name == fHijo.Name)
                    {
                        isopen = true;
                        f.BringToFront();
                        break;
                    }
                }
                else
                {
                    if (f.Name == fHijo.Name)
                    {
                        isopen = false;
                        f.Close();
                        break;
                    }
                }

            }
            if (isopen == false)
            {
                fHijo.TopLevel = false;
                fHijo.Dock = DockStyle.Fill;
                pnl.Controls.Add(fHijo);
                fHijo.Show();
                fHijo.BringToFront();
            }
        }
    }
}
