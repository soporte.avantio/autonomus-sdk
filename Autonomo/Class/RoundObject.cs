﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Class
{
    public class RoundObject
    {
        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        private static extern IntPtr CreateRoundRectRgn
        (
            int LeftRectangle,
            int TopRectangle,
            int RightRectangle,
            int BottomRectangle,
            int WidthElipse,
            int HeightElipse
        );
        public static void RoundForm(Form form, int x, int y)
        {
            form.FormBorderStyle = FormBorderStyle.None;
            form.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, form.Width, form.Height, x, y));
        }
        public static void RoundPanel(Panel panel,  int x, int y)
        {
            panel.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, panel.Width, panel.Height, x, y));
        }
        public static void RoundButton(Button button, int x, int y)
        {
            button.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, button.Width, button.Height, x, y));
        }
        public static void RoundUControl(UserControl uControl, int x, int y)
        {
            uControl.Region = Region.FromHrgn(CreateRoundRectRgn(0, 0, uControl.Width, uControl.Height, x, y));
        }
    }
}
