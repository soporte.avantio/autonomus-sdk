﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Class
{
    public class Validating
    {
        public static void OnlyText(KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar))
                e.Handled = false;
            else if (char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        public static void OnlyNumber(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else
                e.Handled = true;
        }

        public static void OnlyDecimal(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (char.IsSeparator(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString() == ".")
                e.Handled = false;
            else
                e.Handled = true;
        }
        public static void OnlyDate(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString() == "/") //  01/01/2020
                e.Handled = false;
            else if (e.KeyChar.ToString() == "-") // 01-01-2020
                e.Handled = false;
            else
                e.Handled = true;
        }
        public static void OnlyEmail(KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar))
                e.Handled = false;
            if (char.IsLetter(e.KeyChar))
                e.Handled = false;
            else if (char.IsControl(e.KeyChar))
                e.Handled = false;
            else if (e.KeyChar.ToString() == "-") //  algo-mas@mail.com
                e.Handled = false;
            else if (e.KeyChar.ToString() == "_") // algo_mas@mail.com
                e.Handled = false;
            else if (e.KeyChar.ToString() == "@") // algo@mail.com
                e.Handled = false;
            else if (e.KeyChar.ToString() == ".") // algo@mail.com
                e.Handled = false;
            else
                e.Handled = true;
        }
    }
}
