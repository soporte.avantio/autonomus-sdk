﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.Control.HelperControl;

namespace Autonomo.Control
{
    public partial class FlatDateTime : UserControl
    {
        char custom = '/';
        public Color lineMainColor;
        Color focusColor;
        //Agregado nuevo
        bool material = false;
        public FlatDateTime()
        {
            InitializeComponent();
            // Inicializa el control en formato Short
            Format = FormatDate.Short;
            // Iniciar con la fecha actual
            Date.Value = DateTime.Now;
            // Mostrar la fecha actual
            Texto.Text = Date.Value.ToString();
            PlaceHolder.Location = Texto.Location;
            //lineMainColor = Linea.BackColor;
            focusColor = Color.FromArgb(51, 153, 255);

            Titulo.Click += new EventHandler(Titulo_Click);
            Icono.Click += new EventHandler(Titulo_Click);
            Texto.Enter += new EventHandler(Date_Focus);
            Texto.Leave += new EventHandler(Date_Leave);
            HelperControl.InitializeLocationTitle(Icono, Titulo);

            Wrong.Text = string.Empty;
            PlaceHolder.Visible = false;
        }
        #region Espacio: Titulo
        [Category("Autonomo properties")]
        public string Title
        {
            get { return Titulo.Text; }
            set
            {
                value = value ?? string.Empty;
                Titulo.Text = value;
            }
        }
        [Category("Autonomo properties")]
        public Font FontTitle
        {
            get { return Titulo.Font; }
            set { Titulo.Font = value; }
        }
        [Category("Autonomo properties")]

        public Color ColorTitle
        {
            get { return Titulo.ForeColor; }
            set { Titulo.ForeColor = value; }
        }

        [Category("Autonomo properties")]
        public bool VisibleTitle
        {
            get { return Titulo.Visible; }
            set { Titulo.Visible = value; }
        }
        //Agregado nuevo
        [Category("Autonomo properties")]
        public bool MaterialStyle
        {
            get { return material; }
            set { material = value; }
        }
        private void Titulo_Click(object sender, EventArgs e)
        {
            Texto.Focus();
        }
        #endregion
        #region Espacio: Icono
        [Category("Autonomo properties")]
        public DockStyle DockIcon
        {
            get { return Icono.Dock; }
            set
            {
                switch (value)
                {
                    case DockStyle.None:
                        {
                            Icono.Dock = DockStyle.Left;
                            LocationTitle(Icono.Visible);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Top:
                        {
                            Icono.Dock = DockStyle.Left;
                            LocationTitle(Icono.Visible);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Bottom:
                        {
                            Icono.Dock = DockStyle.Left;
                            LocationTitle(Icono.Visible);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Fill:
                        {
                            Icono.Dock = DockStyle.Left;
                            LocationTitle(Icono.Visible);
                            LocationControl();
                            break;
                        }
                    default:
                        {
                            Icono.Dock = value;
                            LocationTitle(Icono.Visible);
                            LocationControl();
                            break;
                        }
                }
            }
        }
        [Category("Autonomo properties")]
        public Image ImageIcon
        {
            get { return Icono.Image; }
            set { Icono.Image = value; Invalidate(); }
        }
        [Category("Autonomo properties")]
        public bool VisibleIcon
        {
            get { return Icono.Visible; }
            set
            {
                LocationTitle(value);
                LocationControl();
                Icono.Visible = value;
            }
        }
        private void LocationTitle(bool visible)
        {
            var dock = Icono.Dock;
            var locationCero = new Point(0, 0);
            var locationSpace = new Point(34, 0);

            if (dock == DockStyle.Right)
            {
                Titulo.Location = locationCero;
            }
            if (dock == DockStyle.Left)
            {
                Titulo.Location = (visible ? locationSpace : locationCero);
            }
        }
        private void LocationControl()
        {
            PlaceHolder.Location = new Point(Titulo.Location.X + 4, Titulo.Location.Y + 18);
            Wrong.Location = new Point(Titulo.Location.X, Botton.Location.Y + 1);
            Help.Location = new Point(Titulo.Location.X, Botton.Location.Y + 1);
        }
        #endregion
        #region Espacio: Linea
        [Category("Autonomo properties")]
        public Color ColorLine
        {
            get { return Linea.BackColor; }
            set { Linea.BackColor = value; }
        }
        [Category("Autonomo properties")]
        public int SizeLine
        {
            get { return Linea.Height; }
            set { Linea.Height = value; }
        }

        [Category("Autonomo properties")]
        public Color ColorFocus // Cambiar el color de la Linea debajo del Texto
        {
            get { return focusColor; }
            set { focusColor = value; }
        }
        #endregion

        #region Espacio:Date
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public override string Text
        {
            get { return Texto.Text; }
            set { Texto.Text = value; }
        }
        [Category("Autonomo properties")]
        public string Error
        {
            get { return Wrong.Text; }
            set
            {
                Wrong.Text = value.Trim();
                //switch (Icono.Dock)
                //{
                //    case DockStyle.Left:
                //        {
                //            Wrong.Text = $"        {value}";
                //            break;
                //        }

                //    case DockStyle.Right:
                //        {
                //            Wrong.Text = value;
                //            break;
                //        }
                //}
            }
        }
        [Category("Autonomo properties")]
        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                Contenedor.BackColor = value;
                Texto.BackColor = value;
                base.BackColor = value;
            }
        }

        [Category("Autonomo properties")]
        public string Info
        {
            get { return Help.Text; }
            set { Help.Text = value; }
        }

        [Category("Autonomo properties")]
        public string Placeholder
        {
            get { return PlaceHolder.Text; }
            set { PlaceHolder.Text = value; }
        }

        /// <summary>
        /// Value: Retorna el Valor seleccioando del DateTimePicker (fecha+hora)
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public DateTime Value
        {
            get { return Date.Value; }
            set { Date.Value = value; }
        }
        /// <summary>
        /// Separa la fecha en caracteres: Ejemplo 01/01/2020, 01-01-2020, 01.01.2020, etc.
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public char DateSeparatorFormat { get { return custom; } set { custom = value; } }
        [Category("Autonomo properties")]
        public Font FontText
        {
            get { return Texto.Font; }
            set { Texto.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorText
        {
            get { return Texto.ForeColor; }
            set { Texto.ForeColor = value; }
        }
        /// <summary>
        /// Modifica el formato de la fecha ( Short y Long)
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public FormatDate Format
        {
            get; set;
        }
        private void _Format()
        {
            switch (Format)
            {
                case FormatDate.Long:
                    {
                        // 00->indica numero (custom-> el carectrer -/)
                        Texto.Mask = $"00{custom}00{custom}0000 00:00:00";
                        Date.Format = DateTimePickerFormat.Long;
                        break;
                    }
                case FormatDate.Short:
                    {
                        Texto.Mask = $"00{custom}00{custom}0000";
                        Date.Format = DateTimePickerFormat.Short;
                        break;
                    }
            }
        }
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Category("Autonomo properties")]
        public int SelectionStart { get { return Texto.SelectionStart; } set { Texto.SelectionStart = value; } }


        //Eventos
        [Browsable(true)]
        [Category("Autonomo properties")]
        public event EventHandler TextBoxChanged;
        public virtual void OnTextChanged()
        {
            if (TextBoxChanged != null)
                this.TextBoxChanged(this, EventArgs.Empty);
        }
        private void Texto_TextChanged(object sender, EventArgs e)
        {
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Texto.Text.Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
            }
            else
            {
                if (Texto.Text.Trim().Length > 0)
                    PlaceHolder.Visible = false;
                else
                    PlaceHolder.Visible = true;
            }
            this.OnTextChanged();
        }

        private void UpDay()
        {
            string value = Texto.Text;
            try
            {
                if (DateTime.TryParse(value, out DateTime now))
                {
                    DateTime newDate = now.AddDays(1);
                    Date.Value = newDate;
                    Texto.Text = newDate.ToShortDateString();
                    return;
                }
                Texto.Text = $"  {(value.Substring(2, 8))}";
            }
            catch (Exception)
            {
                Texto.Text = $"  {(value.Substring(2,8))}";
            }
        }
        private void DownDay()
        {
            string value = Texto.Text;
            try
            {
                if (DateTime.TryParse(value, out DateTime now))
                {
                    DateTime newDate = now.AddDays(-1);
                    Date.Value = newDate;
                    Texto.Text = newDate.ToShortDateString();
                    return;
                }
                Texto.Text = $"  {(value.Substring(2, 8))}";
            }
            catch (Exception)
            {
                Texto.Text = $"  {(value.Substring(2, 8))}";
            }
        }
        private void UpMonth()
        {
            string value = Texto.Text;
            try
            {
                if (DateTime.TryParse(value, out DateTime now))
                {
                    DateTime newDate = now.AddMonths(1);
                    Date.Value = newDate;
                    Texto.Text = newDate.ToShortDateString();
                    return;
                }
                Texto.Text = $"{(value.Substring(0, 3))}  {(value.Substring(3, 7))}";
            }
            catch (Exception)
            {
                Texto.Text = $"{(value.Substring(0,3))}  {(value.Substring(3, 7))}";
            }
        }
        private void DownMonth()
        {
            string value = Texto.Text;
            try
            {
                if (DateTime.TryParse(value, out DateTime now))
                {
                    DateTime newDate = now.AddMonths(-1);
                    Date.Value = newDate;
                    Texto.Text = newDate.ToShortDateString();
                    return;
                }
                Texto.Text = $"{(value.Substring(0, 3))}  {(value.Substring(3, 7))}";
            }
            catch (Exception)
            {
                Texto.Text = $"{(value.Substring(0, 3))}  {(value.Substring(3, 7))}";
            }
        }
        private void UpYear()
        {
            string value = Texto.Text;
            try
            {
                if (DateTime.TryParse(value, out DateTime now))
                {
                    DateTime newDate = now.AddYears(1);
                    Date.Value = newDate;
                    Texto.Text = newDate.ToShortDateString();
                    return;
                }
                Texto.Text = value;
            }
            catch (Exception)
            {
                Texto.Text = $"{(value.Substring(0, 6))}    ";
            }
        }
        private void DownYear()
        {
            string value = Texto.Text;
            try
            {
                if (DateTime.TryParse(value, out DateTime now))
                {
                    DateTime newDate = now.AddYears(-1);
                    Date.Value = newDate;
                    Texto.Text = newDate.ToShortDateString();
                    return;
                }
                Texto.Text = value;
            }
            catch (Exception)
            {
                Texto.Text = $"{(value.Substring(0, 6))}    ";
            }
        }

        [Browsable(true)]
        [Category("Autonomo properties")]
        public event EventHandler DateChange;
        public virtual void OnDateChange()
        {
            if (DateChange != null)
                this.DateChange(this, EventArgs.Empty);
        }
        private void Date_ValueChanged(object sender, EventArgs e)
        {
            Texto.Text = Date.Value.ToShortDateString();//
            this.OnDateChange();
        }

        [Category("Autonomo properties")]
        public new event EventHandler<KeyEventArgs> KeyDown; // ejecuta alguna función mientras se presiona una tecla en especificco...
        protected override void OnKeyDown(KeyEventArgs e)
        {
            var handler = KeyDown;
            if (handler != null) handler(this, e);
        }
        private void Texto_KeyDown(object sender, KeyEventArgs e)
        {
            this.OnKeyDown(e);
            #region date-KeysUp
            int pos = Texto.SelectionStart;
            if (e.KeyCode == Keys.Up)
            {
                if (pos >= 0 && pos <= 2)
                {
                    UpDay();
                    Texto.SelectionStart = 1;
                }
                if (pos >= 3 && pos <= 5)
                {
                    UpMonth();
                    Texto.SelectionStart = 4;
                }
                if (pos >= 6 && pos <= 10)
                {
                    UpYear();
                    Texto.SelectionStart = 7;
                }
            }
            if (e.KeyCode == Keys.Down)
            {
                if (pos >= 0 && pos <= 2)
                {
                    DownDay();
                    Texto.SelectionStart = 1;
                }
                if (pos >= 3 && pos <= 5)
                {
                    DownMonth();
                    Texto.SelectionStart = 4;
                }
                if (pos >= 6 && pos <= 10)
                {
                    DownYear();
                    Texto.SelectionStart = 9;
                }
            }
            #endregion

            #region Date-KeysBack
            if (e.KeyCode == Keys.Back)
            {
                if (pos >= 0 && pos <= 2)
                {
                    Texto.Focus();
                    Texto.Select(0, 2);
                }
                if (pos >= 3 && pos <= 5)
                {
                    Texto.Focus();
                    Texto.Select(3, 2);
                }
                if (pos >= 6 && pos <= 10)
                {
                    UpYear();
                    Texto.SelectionStart = 7;
                }
            }
            #endregion
        }

        private void ReplaceDateChar(string value)
        {
            for (int i = 0; i < value.Length; i++)
            {

            }
        }

        #endregion

        #region Acciones Activadas()
        private void FlatDateTime_Load(object sender, EventArgs e)
        {
            _Format();
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            lineMainColor = Linea.BackColor;
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Texto.Text.Trim().Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    PlaceHolder.Text = Titulo.Text;
                    PlaceHolder.Font = Titulo.Font;
                    PlaceHolder.ForeColor = Titulo.ForeColor;
                    Titulo.Visible = false;
                }
            }
            else
            {
                if (Texto.Text.Trim().Length > 0)
                {
                    PlaceHolder.Visible = false;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    PlaceHolder.ForeColor = Titulo.ForeColor;
                }
            }
        }
        /// <summary>
        /// Al darle Clik en el Label DropDown, se desplazará un calendario para seleccionar la fecha
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Drop_Click(object sender, EventArgs e)
        {
            Date.Select();
            SendKeys.Send("%{DOWN}");
        }
        private void Texto_Click(object sender, EventArgs e)
        {
            BackToFirstPosition(Texto);
        }
        private void Texto_Leave(object sender, EventArgs e)
        {
            ValidateFormat(Texto, Date, Format);
        }

        #endregion

        private void Wrong_TextChanged(object sender, EventArgs e)
        {
            if (Wrong.Text.Trim().Length > 0)
            { Help.Visible = false; }
            else { Help.Visible = true; }
        }
        private void Date_Focus(object sender, EventArgs e)
        {
            Linea.BackColor = focusColor;
            //Agregado nuevo
            if (MaterialStyle)
            {
                PlaceHolder.Visible = false;
                Titulo.Visible = true;
            }
        }
        private void Date_Leave(object sender, EventArgs e)
        {
            Linea.BackColor = lineMainColor;
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Texto.Text.Trim().Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    Titulo.Visible = false;
                }
            }
            else
            {
                if (Texto.Text.Trim().Length > 0)
                    PlaceHolder.Visible = false;
                else
                    PlaceHolder.Visible = true;
            }
        }

        private void PlaceHolder_Click(object sender, EventArgs e)
        {
            Texto.Focus();
        }
    }
}
