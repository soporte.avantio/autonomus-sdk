﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public class CustomGrid : DataGridView
    {
        public CustomGrid()
        {
            // Hacemos que el fondo de la Grilla sea de color Blanco
            base.BackgroundColor = Color.White;
            //Quitamos el Borde por defecto; dejamos una grilla sin borde
            base.BorderStyle = BorderStyle.None;
            // Quitamos el borde de las Celdas
            base.CellBorderStyle = DataGridViewCellBorderStyle.None;
            // Quitamos el borde de la cabecera
            base.ColumnHeadersBorderStyle = DataGridViewHeaderBorderStyle.None;
            // Desabilitar el cambio del Tamaño de la cabecera
            //base.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            // Establecer la altura de la cabecera
            base.ColumnHeadersHeight = 34;
            // Deshabilitar la vista de Estilo: para poner color personalizado a la cabecera.
            base.EnableHeadersVisualStyles = false;
            // Ocultar la columna de indices por defecto
            base.RowHeadersVisible = false;
            // Evita crear nuevas filas al editar. (En Ejecucion)
            base.AllowUserToAddRows = false;
            // Evita eliminar Filas de la grilla. (En Ejecucion)
            base.AllowUserToDeleteRows = false;
            // Evita cambiar el tamañano de las columnas (En Ejecucion)
            base.AllowUserToResizeColumns = true;
            // Evita Cambiar el tamaño de las Filas (En Ejecucion)
            base.AllowUserToResizeRows = false;
            // Evita hacer selecciones multiples
            base.MultiSelect = false;
            // Habilitar la selección de la Fila completa
            base.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            // 

            // 
            this.ColumnHeadersHeight = 34;
            //this.ReadOnly = true;
            //this.ColumnHeadersHeightSizeMode = DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
        }
        #region header
        [Category("Autonomo properties")]
        public Color HeaderColor
        {
            get { return base.ColumnHeadersDefaultCellStyle.BackColor; }
            set
            {
                base.ColumnHeadersDefaultCellStyle.BackColor = value;
                base.ColumnHeadersDefaultCellStyle.SelectionBackColor = value;
            }
        }
        [Category("Autonomo properties")]
        public Color HeaderForeColor
        {
            get { return base.ColumnHeadersDefaultCellStyle.ForeColor; }
            set
            {
                base.ColumnHeadersDefaultCellStyle.SelectionForeColor = value;
                base.ColumnHeadersDefaultCellStyle.ForeColor = value;
            }
        }
        [Category("Autonomo properties")]
        public Font HeaderFont
        {
            get { return base.ColumnHeadersDefaultCellStyle.Font; }
            set { base.ColumnHeadersDefaultCellStyle.Font = value; }
        }
        #endregion
        #region Body
        [Category("Autonomo properties")]
        public Color BodySelectColor
        {
            get { return base.DefaultCellStyle.SelectionBackColor; }
            set { base.DefaultCellStyle.SelectionBackColor = value; }
        }
        [Category("Autonomo properties")]
        public Color BodyForeColor
        {
            get { return base.DefaultCellStyle.ForeColor; }
            set { base.DefaultCellStyle.ForeColor = value; }
        }
        [Category("Autonomo properties")]
        public Font BodyFont
        {
            get { return base.DefaultCellStyle.Font; }
            set { base.DefaultCellStyle.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color BodySelectForeColor
        {
            get { return base.DefaultCellStyle.SelectionForeColor; }
            set { base.DefaultCellStyle.SelectionForeColor = value; }
        }
        [Category("Autonomo properties")]
        public Color CellStyleBackColor
        {
            get { return base.DefaultCellStyle.BackColor; }
            set { base.DefaultCellStyle.BackColor = value; }
        }
        //[Category("Autonomo properties")]
        //public Color
        #endregion
        #region Additional
        [Category("Autonomo properties")]
        public new DataGridViewColumnHeadersHeightSizeMode ColumnHeadersHeightSizeMode
        {
            get { return base.ColumnHeadersHeightSizeMode; }
            set { base.ColumnHeadersHeightSizeMode = value; }
        }
        [Category("Autonomo properties")]
        public override DataGridViewAdvancedBorderStyle AdjustColumnHeaderBorderStyle(DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStyleInput, DataGridViewAdvancedBorderStyle dataGridViewAdvancedBorderStylePlaceholder, bool isFirstDisplayedColumn, bool isLastVisibleColumn)
        {
            return base.AdjustColumnHeaderBorderStyle(dataGridViewAdvancedBorderStyleInput, dataGridViewAdvancedBorderStylePlaceholder, isFirstDisplayedColumn, isLastVisibleColumn);
        }

        [Category("Autonomo properties")]
        public override void Sort(DataGridViewColumn dataGridViewColumn, ListSortDirection direction)
        {
            base.Sort(dataGridViewColumn, direction);
        }
        [Category("Autonomo properties")]
        public new DataGridViewSelectionMode SelectionMode
        {
            get { return base.SelectionMode; }
            set { base.SelectionMode = value; }
        }
        [Category("Autonomo properties")]
        public new bool MultiSelect
        {
            get { return base.MultiSelect; }
            set { base.MultiSelect = value; }
        }
        

        #endregion
        #region Function
        protected override void OnTabStopChanged(EventArgs e)
        {
            //base.TabStop = false;
            base.OnTabStopChanged(e);
        }
        protected override bool ProcessDialogKey(Keys keyData)
        {
            if (keyData == Keys.Enter)
            {
                int col = this.CurrentCell.ColumnIndex;
                int row = this.CurrentCell.RowIndex;
                this.CurrentCell = this[col, row];
                return true;
            }
            return base.ProcessDialogKey(keyData);
        }

        protected override void OnKeyDown(KeyEventArgs e)
        {
            if (e.KeyData == Keys.Enter)
            {
                int col = this.CurrentCell.ColumnIndex;
                int row = this.CurrentCell.RowIndex;
                this.CurrentCell = this[col, row];
                e.Handled = true;
            }

            if (this.Rows.Count > 0)
            {
                int col = this.CurrentCell.ColumnIndex;
                int row = this.CurrentCell.RowIndex;

                int lastCol = this.ColumnCount - 1;
                if (e.KeyCode == Keys.Right)
                {
                    if (col == lastCol)
                    {
                        GoToFirstColumn(row);
                        e.Handled = true;
                    }
                }
                if (e.KeyCode == Keys.Left)
                {
                    if (col == 0) 
                    {
                        GoToLastColumn(lastCol, row);
                        e.Handled = true;
                    }
                }

                int lastRow = this.RowCount - 1;
                if (e.KeyCode == Keys.Up)
                {
                    if (row == 0) 
                    {
                        GoToLastRow(col, lastRow);
                        e.Handled = true;
                    }
                }
                if (e.KeyCode == Keys.Down)
                {
                    if (row == lastRow) 
                    {
                        GoToFirstRow(col);
                        e.Handled = true;
                    }
                }
            }
            base.OnKeyDown(e);
        }
        private void GoToFirstColumn(int row)
        {
            this[0, row].Selected = true;
        }
        private void GoToLastColumn(int lastCol, int row)
        {
            this[lastCol, row].Selected = true;
        }
        private void GoToFirstRow(int col)
        {
            this[col, 0].Selected = true;
        }
        private void GoToLastRow(int col, int lastRow)
        {
            this[col, lastRow].Selected = true;
        }

        #endregion
    }
}
