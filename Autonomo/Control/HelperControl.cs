﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public class HelperControl
    {
        public static void LocationTitle(bool visibleIcon, Label Icono, Label Titulo)
        {
            var dock = Icono.Dock; // Obtenemos la posicion actual
            var locationCero = new Point(0, 0); // Establece la posición 0,0
            var locationSpace = new Point(Icono.Width, 0); // Establece la posición 34,0

            var holderCero = new Point(3, 18);
            var holderSpace = new Point(Icono.Width, 18);

            if (dock == DockStyle.Right)
            {
                Titulo.Location = locationCero;
            }
            if (dock == DockStyle.Left)
            {
                Titulo.Location = (visibleIcon ? locationSpace : locationCero);
            }
        }
        
        public static void InitializeLocationTitle(Label Icono, Label Titulo)
        {
            Titulo.Location = (Icono.Visible && Icono.Dock == DockStyle.Left
                ? new Point(Icono.Width, 0) : new Point(0, 0));
        }

        // Enumerable para dar formato al la fecha (Long y Short)
        public enum FormatDate { Long, Short }

        public static void BackToFirstPosition(MaskedTextBox text)
        {
           // text.SelectionStart = 0;
            text.Text = text.Text.Replace(" ", "");
            //text.SelectAll();
            text.Focus();
        }
        public static void ValidateFormat(MaskedTextBox text, DateTimePicker date, FormatDate format)
        {
            if ((text.Text.Trim().Length > 9 && format == FormatDate.Short) || (text.Text.Trim().Length > 18 && format == FormatDate.Long))
            {
                if (DateTime.TryParse(text.Text, out DateTime _date)) { date.Value = _date; }
                else
                { BackToFirstPosition(text); }
            }
        }
    }
}
