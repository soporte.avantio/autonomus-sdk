﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public partial class FlatComboBox : UserControl
    {
        public Color lineMainColor;
        Color focusColor;
        //Agregado nuevo
        bool material = false;
        public FlatComboBox()
        {
            InitializeComponent();
            Titulo.Click += new EventHandler(Titulo_Click);
            Icono.Click += new EventHandler(Titulo_Click);
            HelperControl.InitializeLocationTitle(Icono, Titulo);
            Wrong.Text = string.Empty;
            PlaceHolder.Location = Combo.Location;
            // lineMainColor = Linea.BackColor;
            focusColor = Color.FromArgb(51, 153, 255);
            Combo.Enter += new EventHandler(Combo_Focus);
            Combo.Leave += new EventHandler(Combo_Leave);
        }
        #region Espacio: Titulo
        [Category("Autonomo properties")]
        public string Title
        {
            get { return Titulo.Text; }
            set
            {
                value = value ?? string.Empty; // Validamos si el valor es nulo
                Titulo.Text = value;// Llenamos el Text con el valor final.
            }
        }
        [Category("Autonomo properties")]
        public Font FontTitle //Cambia el tipo de fuente de l Titulo
        {
            get { return Titulo.Font; }
            set { Titulo.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorTitle // Cambia el color del texto del Titulo
        {
            get { return Titulo.ForeColor; }
            set { Titulo.ForeColor = value; }
        }
        [Category("Autonomo properties")]
        public bool VisibleTitle //Controla la visibilidad del titulo
        {
            get { return Titulo.Visible; }
            set { Titulo.Visible = value; }
        }
        //Agregado nuevo
        [Category("Autonomo properties")]
        public bool MaterialStyle
        {
            get { return material; }
            set { material = value; }
        }
        private void Titulo_Click(object sender, EventArgs e)
        {
            Combo.Focus();
        }

        #endregion
        #region Espacio: Icono
        [Category("Autonomo properties")]
        public DockStyle DockIcon // Para establecer la posición del icono
        {
            get { return Icono.Dock; }
            set
            {
                switch (value)
                {
                    case DockStyle.None:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Top:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Bottom:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Fill:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    default:
                        {
                            Icono.Dock = value;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                }
            }
        }
        private void LocationControl()
        {
            PlaceHolder.Location = new Point(Titulo.Location.X + 4, Titulo.Location.Y + 18);
            Wrong.Location = new Point(Titulo.Location.X, Botton.Location.Y + 1);
            Help.Location = new Point(Titulo.Location.X, Botton.Location.Y + 1);
        }

        [Category("Autonomo properties")]
        public Image ImageIcon // Establece la imagén del icono
        {
            get { return Icono.Image; }
            set { Icono.Image = value; Invalidate(); }
        }
        [Category("Autonomo properties")]
        public bool VisibleIcon
        {
            get { return Icono.Visible; }
            set
            {
                HelperControl.LocationTitle(value, Icono, Titulo);
                LocationControl();
                Icono.Visible = value;
            }
        }
        #endregion
        #region Espacio: Linea
        [Category("Autonomo properties")]
        public Color ColorLine // Cambiar el color de la Linea debajo del Texto
        {
            get { return Linea.BackColor; }
            set { Linea.BackColor = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorFocus // Cambiar el color de la Linea debajo del Texto
        {
            get { return focusColor; }
            set { focusColor = value; }
        }

        [Category("Autonomo properties")]
        public int SizeLine // Cambia la altura de la Linea
        {
            get { return Linea.Height; }
            set { Linea.Height = value; }
        }
        #endregion
        #region Espacio: Combo
        /// <summary>
        /// Text Muestra el valor que se ha seleccionado en el ComboBox
        /// </summary>
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        //[Category("Appearance")]
        [Category("Autonomo properties")]
        [Localizable(true)]
        public override string Text { get { return Combo.Text; } set { Combo.Text = value; } }
        [Category("Autonomo properties")]
        public string Error
        {
            get { return Wrong.Text; }
            set
            {
                Wrong.Text = value.Trim();
                //switch (Icono.Dock)
                //{
                //    case DockStyle.Left:
                //        {
                //            Wrong.Text = $"        {value}";
                //            break;
                //        }

                //    case DockStyle.Right:
                //        {
                //            Wrong.Text = value;
                //            break;
                //        }
                //}
            }
        }
        [Category("Autonomo properties")]
        public string Info
        {
            get { return Help.Text; }
            set { Help.Text = value; }
        }

        [Category("Autonomo properties")]
        public string Placeholder
        {
            get { return PlaceHolder.Text; }
            set { PlaceHolder.Text = value; }
        }

        [Category("Autonomo properties")]
        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                Contenedor.BackColor = value;
                Combo.BackColor = value;
                base.BackColor = value;
            }
        }

        /// <summary>
        /// Enabled personalizado; Oculta el ComboBox y muestra una etiqueta con el valor seleccionado
        /// </summary>
        ///   [Category("Autonomo properties")]
        public new bool Enabled
        {
            get { return Combo.Visible; }
            set
            {
                Marca.Text = Combo.Text;
                Marca.Visible = !value;
                Contenedor.Enabled = value;
                Combo.Visible = value;
            }
        }
        /// <summary>
        /// DropDownStyle Cambia la forma del despliegue del ComboBox
        /// </summary>
        ///   [Category("Autonomo properties")]
        public System.Windows.Forms.ComboBoxStyle DropDownStyle
        {
            get { return Combo.DropDownStyle; }
            set { Combo.DropDownStyle = value; }
        }
        /// <summary>
        /// FontText Cambia el tipo de fuente del Control
        /// </summary>
        ///   [Category("Autonomo properties")]
        public Font FontText
        {
            get { return Combo.Font; }
            set { Combo.Font = value; }
        }
        /// <summary>
        /// ColorText cambia el color del texto del control
        /// </summary>
        ///   [Category("Autonomo properties")]
        public Color ColorText
        {
            get { return Combo.ForeColor; }
            set { Combo.ForeColor = value; }
        }
        /// <summary>
        /// Focus Se sobrepone ante otros controles::: hace Focus()
        /// </summary>
        ///   [Category("Autonomo properties")]
        public new void Focus()
        {
            Combo.Focus();
        }
        /// <summary>
        /// Items: Adiciona el combobox desde una colección de objetos.
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public System.Windows.Forms.ComboBox.ObjectCollection Items
        {
            set
            {
                // Iteramos la colección para adicionar más items al combobox
                for (byte i = 0; i < value.Count; i++)
                    Combo.Items.Add(value[i].ToString());
            }
            get { return Combo.Items; } // Retorna los Items de combobox
        }
        /// <summary>
        /// DataSource Recibe una lista de datos desde un DataTable o List, etc.
        /// </summary>
        /// 

        [AttributeProvider(typeof(IListSource))]
        [DefaultValue(null)]
        [RefreshProperties(RefreshProperties.Repaint)]
        [Category("Autonomo properties")]
        public object DataSource
        {
            get { return Combo.DataSource; }
            set { Combo.DataSource = value; }
        }
        /// <summary>
        /// ValueMember Almacena la llave o Key de la data en el ComboBox
        /// </summary>
        [DefaultValue(null)]
        [Category("Autonomo properties")]
        public string ValueMember
        {
            get { return Combo.ValueMember; }
            set { Combo.ValueMember = value; }
        }
        /// <summary>
        /// DisplayMember: Muestra la información de la lista - datasource.
        /// </summary>
        [DefaultValue(null)]
        [Category("Autonomo properties")]
        public string DisplayMember
        {
            get { return Combo.DisplayMember; }
            set { Combo.DisplayMember = value; }
        }

        /// <summary>
        /// Sorted: 
        /// </summary>
        [DefaultValue(false)]
        [Category("Autonomo properties")]
        public bool Sorted
        {
            get { return Combo.Sorted; }
            set { Combo.Sorted = value; }
        }
        /// <summary>
        /// SelectedValue: Selecciona el Valor(Key) del ComboBox
        /// </summary>
        [Bindable(true)]
        [Browsable(false)]
        [DefaultValue(null)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Category("Autonomo properties")]
        public object SelectedValue
        {
            get
            {
                string valor = string.Empty;
                if (Combo.SelectedValue != null)
                { valor = Combo.SelectedValue.ToString(); }
                return valor;
            }
            set { Combo.SelectedValue = value; }
        }

        /// <summary>
        /// SelectedIndex: Selecciona el Índice del dato  en el ComboBox
        /// </summary>
        [Category("Autonomo properties")]
        public int SelectedIndex
        {
            get { return Combo.SelectedIndex; }
            set
            {
                Combo.SelectedIndex = value;
            }
        }
        /// <summary>
        /// FindString: Solicita un valor y retorna el valor del ínidice en donde se encuentra
        /// </summary>
        /// <param name="s"> valor que se va a buscar </param>
        /// <returns></returns>
        [Category("Autonomo properties")]
        public int FindString(string s)
        {
            int index = Combo.FindString(s);
            return index;
        }
        /// <summary>
        /// SelectedText: Selecciona el Texto del ComboBox
        /// </summary>
        [Browsable(false)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
        [Category("Autonomo properties")]
        public string SelectedText
        {
            get { return Combo.SelectedText; }
            set { Combo.SelectedText = value; }
        }
        //Eventos()
        /// <summary>
        /// SelectedValueChanged: Ejecuta una acción cuandocambia el valor seleccionado (key).
        /// </summary>
        [Browsable(true)]
        [Category("Autonomo properties")]
        public event EventHandler SelectedValueChanged;
        public virtual void OnSelectedValueChanged()
        {
            if (SelectedValueChanged != null)
                this.SelectedValueChanged(this, EventArgs.Empty);
        }
        private void Combo_SelectedValueChanged(object sender, EventArgs e)
        {
            if (Combo.SelectedValue != null)
                this.OnSelectedValueChanged();
        }
        /// <summary>
        /// SelectedTextChanged: Ejecuta una acción cuando cambia el valor del texto
        /// </summary>
        [Browsable(true)]
        [Category("Autonomo properties")]
        public event EventHandler SelectedTextChanged;
        public virtual void OnSelectedTextChanged()
        {
            if (SelectedTextChanged != null)
                this.SelectedTextChanged(this, EventArgs.Empty);
        }
        private void Combo_TextChanged(object sender, EventArgs e)
        {
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Combo.Text.Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
            }
            else
            {
                if (Combo.Text.Trim().Length > 0)
                    PlaceHolder.Visible = false;
                else
                    PlaceHolder.Visible = true;
            }

            if (Combo.Text != null)
                this.OnSelectedTextChanged();
        }
        /// <summary>
        /// SelectedIndexChanged: Ejecuta una acción  cuando cambia el indice del item seleccionado.
        /// </summary>
        [Browsable(true)]
        [Category("Autonomo properties")]
        public event EventHandler SelectedIndexChanged;
        public virtual void OnSelectedIndexChanged()
        {
            if (SelectedIndexChanged != null)
                this.SelectedIndexChanged(this, EventArgs.Empty);
        }
        private void Combo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Combo.SelectedIndex >= 0)
                this.OnSelectedIndexChanged();
        }
        #endregion

        private void Wrong_TextChanged(object sender, EventArgs e)
        {
            if (Wrong.Text.Trim().Length > 0)
            { Help.Visible = false; }
            else { Help.Visible = true; }
        }

        private void PlaceHolder_Click(object sender, EventArgs e)
        {
            Combo.Focus();
        }
        private void Combo_Focus(object sender, EventArgs e)
        {
            Linea.BackColor = focusColor;
            //Agregado nuevo
            if (MaterialStyle)
            {
                PlaceHolder.Visible = false;
                Titulo.Visible = true;
            }
        }
        private void Combo_Leave(object sender, EventArgs e)
        {
            Linea.BackColor = lineMainColor;
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Combo.Text.Trim().Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    Titulo.Visible = false;
                }
            }
            else
            {
                if (Combo.Text.Trim().Length > 0)
                    PlaceHolder.Visible = false;
                else
                    PlaceHolder.Visible = true;
            }
        }

        private void FlatComboBox_Load(object sender, EventArgs e)
        {
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            lineMainColor = Linea.BackColor;
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Combo.Text.Trim().Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    PlaceHolder.Text = Titulo.Text;
                    PlaceHolder.Font = Titulo.Font;
                    PlaceHolder.ForeColor = Titulo.ForeColor;
                    Titulo.Visible = false;
                }
            }
            else
            {
                if (Combo.Text.Trim().Length > 0)
                {
                    PlaceHolder.Visible = false;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    PlaceHolder.ForeColor = Titulo.ForeColor;
                }
            }
        }
    }
}
