﻿using Microsoft.VisualBasic.Compatibility.VB6;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public class CustomCheck : CheckBox
    {
        private Image uImage = Properties.Resources.UnCheckBox;
        private Image cImage = Properties.Resources.CheckBox;
        public CustomCheck()
        {
            base.Appearance = Appearance.Button;
            base.FlatAppearance.BorderSize = 0;
            base.FlatAppearance.MouseDownBackColor = Color.Transparent;
            base.FlatAppearance.MouseOverBackColor = Color.Transparent;
            base.Cursor = Cursors.Hand;
            base.FlatStyle = FlatStyle.Flat;
            base.Font = new Font("Verdana", 10, FontStyle.Regular);
            base.Image = uImage;
            base.TextImageRelation = TextImageRelation.ImageBeforeText;
            this.CheckedChanged += new EventHandler(Ckeck_CheckedChanged);
        }

        public Image ImageChecking { get { return cImage; } set { cImage = value; } }
        public Image ImageUnChecking { get { return uImage; } set { uImage = value; } }

        private void Ckeck_CheckedChanged(object sender, EventArgs e)
        {
            var value = base.Checked;
            base.Image = value ? cImage : uImage;
        }
    }
}
