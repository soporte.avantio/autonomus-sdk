﻿namespace Autonomo.Control
{
    partial class FlatComboBox
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlatComboBox));
            this.Contenedor = new System.Windows.Forms.Panel();
            this.PlaceHolder = new System.Windows.Forms.Label();
            this.Combo = new System.Windows.Forms.ComboBox();
            this.Marca = new System.Windows.Forms.Label();
            this.Titulo = new System.Windows.Forms.Label();
            this.arriba = new System.Windows.Forms.Label();
            this.abajo = new System.Windows.Forms.Label();
            this.izquierda = new System.Windows.Forms.Label();
            this.Linea = new System.Windows.Forms.Label();
            this.Icono = new System.Windows.Forms.Label();
            this.Wrong = new System.Windows.Forms.Label();
            this.Botton = new System.Windows.Forms.Label();
            this.Help = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.BackColor = System.Drawing.Color.Transparent;
            this.Contenedor.Controls.Add(this.PlaceHolder);
            this.Contenedor.Controls.Add(this.Combo);
            this.Contenedor.Controls.Add(this.Marca);
            this.Contenedor.Controls.Add(this.Titulo);
            this.Contenedor.Controls.Add(this.arriba);
            this.Contenedor.Controls.Add(this.abajo);
            this.Contenedor.Controls.Add(this.izquierda);
            this.Contenedor.Controls.Add(this.Linea);
            this.Contenedor.Controls.Add(this.Icono);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 0);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(278, 44);
            this.Contenedor.TabIndex = 0;
            // 
            // PlaceHolder
            // 
            this.PlaceHolder.AutoSize = true;
            this.PlaceHolder.BackColor = System.Drawing.Color.Transparent;
            this.PlaceHolder.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.PlaceHolder.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceHolder.ForeColor = System.Drawing.Color.DarkGray;
            this.PlaceHolder.Location = new System.Drawing.Point(46, 18);
            this.PlaceHolder.Name = "PlaceHolder";
            this.PlaceHolder.Size = new System.Drawing.Size(0, 16);
            this.PlaceHolder.TabIndex = 0;
            this.PlaceHolder.Click += new System.EventHandler(this.PlaceHolder_Click);
            // 
            // Combo
            // 
            this.Combo.BackColor = System.Drawing.Color.White;
            this.Combo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Combo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.Combo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Combo.Font = new System.Drawing.Font("Verdana", 10F);
            this.Combo.FormattingEnabled = true;
            this.Combo.Location = new System.Drawing.Point(39, 18);
            this.Combo.Name = "Combo";
            this.Combo.Size = new System.Drawing.Size(239, 24);
            this.Combo.TabIndex = 0;
            this.Combo.SelectedIndexChanged += new System.EventHandler(this.Combo_SelectedIndexChanged);
            this.Combo.SelectedValueChanged += new System.EventHandler(this.Combo_SelectedValueChanged);
            this.Combo.TextChanged += new System.EventHandler(this.Combo_TextChanged);
            // 
            // Marca
            // 
            this.Marca.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Marca.Font = new System.Drawing.Font("Verdana", 10F);
            this.Marca.Location = new System.Drawing.Point(39, 18);
            this.Marca.Name = "Marca";
            this.Marca.Size = new System.Drawing.Size(239, 22);
            this.Marca.TabIndex = 0;
            this.Marca.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Marca.Visible = false;
            // 
            // Titulo
            // 
            this.Titulo.AutoSize = true;
            this.Titulo.Font = new System.Drawing.Font("Verdana", 9F);
            this.Titulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Titulo.Location = new System.Drawing.Point(36, 0);
            this.Titulo.Name = "Titulo";
            this.Titulo.Size = new System.Drawing.Size(156, 14);
            this.Titulo.TabIndex = 0;
            this.Titulo.Text = "Identificador del control";
            // 
            // arriba
            // 
            this.arriba.Dock = System.Windows.Forms.DockStyle.Top;
            this.arriba.Font = new System.Drawing.Font("Verdana", 10F);
            this.arriba.Location = new System.Drawing.Point(39, 0);
            this.arriba.Name = "arriba";
            this.arriba.Size = new System.Drawing.Size(239, 18);
            this.arriba.TabIndex = 0;
            // 
            // abajo
            // 
            this.abajo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.abajo.Font = new System.Drawing.Font("Verdana", 10F);
            this.abajo.Location = new System.Drawing.Point(39, 40);
            this.abajo.Name = "abajo";
            this.abajo.Size = new System.Drawing.Size(239, 2);
            this.abajo.TabIndex = 0;
            // 
            // izquierda
            // 
            this.izquierda.Dock = System.Windows.Forms.DockStyle.Left;
            this.izquierda.Font = new System.Drawing.Font("Verdana", 10F);
            this.izquierda.Location = new System.Drawing.Point(36, 0);
            this.izquierda.Name = "izquierda";
            this.izquierda.Size = new System.Drawing.Size(3, 42);
            this.izquierda.TabIndex = 0;
            // 
            // Linea
            // 
            this.Linea.BackColor = System.Drawing.Color.Gray;
            this.Linea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Linea.Font = new System.Drawing.Font("Verdana", 10F);
            this.Linea.Location = new System.Drawing.Point(36, 42);
            this.Linea.Name = "Linea";
            this.Linea.Size = new System.Drawing.Size(242, 2);
            this.Linea.TabIndex = 0;
            // 
            // Icono
            // 
            this.Icono.Dock = System.Windows.Forms.DockStyle.Left;
            this.Icono.Font = new System.Drawing.Font("Verdana", 10F);
            this.Icono.Image = ((System.Drawing.Image)(resources.GetObject("Icono.Image")));
            this.Icono.Location = new System.Drawing.Point(0, 0);
            this.Icono.Name = "Icono";
            this.Icono.Size = new System.Drawing.Size(36, 44);
            this.Icono.TabIndex = 0;
            // 
            // Wrong
            // 
            this.Wrong.AutoSize = true;
            this.Wrong.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Wrong.ForeColor = System.Drawing.Color.Red;
            this.Wrong.Location = new System.Drawing.Point(36, 44);
            this.Wrong.Name = "Wrong";
            this.Wrong.Size = new System.Drawing.Size(0, 13);
            this.Wrong.TabIndex = 0;
            this.Wrong.TextChanged += new System.EventHandler(this.Wrong_TextChanged);
            // 
            // Botton
            // 
            this.Botton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Botton.Location = new System.Drawing.Point(0, 44);
            this.Botton.Name = "Botton";
            this.Botton.Size = new System.Drawing.Size(278, 14);
            this.Botton.TabIndex = 0;
            // 
            // Help
            // 
            this.Help.AutoSize = true;
            this.Help.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Help.ForeColor = System.Drawing.Color.DarkGray;
            this.Help.Location = new System.Drawing.Point(36, 44);
            this.Help.Name = "Help";
            this.Help.Size = new System.Drawing.Size(0, 13);
            this.Help.TabIndex = 0;
            // 
            // FlatComboBox
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Help);
            this.Controls.Add(this.Wrong);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.Botton);
            this.Name = "FlatComboBox";
            this.Size = new System.Drawing.Size(278, 58);
            this.Load += new System.EventHandler(this.FlatComboBox_Load);
            this.Contenedor.ResumeLayout(false);
            this.Contenedor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.Label Titulo;
        private System.Windows.Forms.Label arriba;
        private System.Windows.Forms.Label abajo;
        private System.Windows.Forms.Label izquierda;
        private System.Windows.Forms.Label Linea;
        private System.Windows.Forms.Label Icono;
        public System.Windows.Forms.ComboBox Combo;
        private System.Windows.Forms.Label Marca;
        public System.Windows.Forms.Label Wrong;
        private System.Windows.Forms.Label Botton;
        public System.Windows.Forms.Label Help;
        private System.Windows.Forms.Label PlaceHolder;
    }
}
