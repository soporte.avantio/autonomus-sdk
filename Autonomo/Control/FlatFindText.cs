﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public partial class FlatFindText : UserControl
    {
        public Color lineMainColor;
        Color focusColor;
        private string _idText = "";
        //Agregado nuevo
        bool material = false;
        public FlatFindText()
        {
            InitializeComponent();
            PlaceHolder.Location = Texto.Location;
            //lineMainColor = Linea.BackColor;
            focusColor = Color.FromArgb(51, 153, 255);
            Texto.Enter += new EventHandler(Find_Focus);
            Texto.Leave += new EventHandler(Find_Leave);
            Titulo.Click += new EventHandler(Titulo_Click);
            Icono.Click += new EventHandler(Titulo_Click);
            Icono.Click += new EventHandler(button_Click);
            HelperControl.InitializeLocationTitle(Icono, Titulo);
            PlaceHolder.Visible = false;
        }
        #region Espacio: Titulo
        [Category("Autonomo properties")]
        public string Title
        {
            get { return Titulo.Text; }
            set
            {
                value = value ?? string.Empty; // Validamos si el valor es nulo
                Titulo.Text = value;// Llenamos el Text con el valor final.
            }
        }
        [Category("Autonomo properties")]
        public Font FontTitle //Cambia el tipo de fuente de l Titulo
        {
            get { return Titulo.Font; }
            set { Titulo.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorTitle // Cambia el color del texto del Titulo
        {
            get { return Titulo.ForeColor; }
            set { Titulo.ForeColor = value; }
        }
        [Category("Autonomo properties")]
        public bool VisibleTitle //Controla la visibilidad del titulo
        {
            get { return Titulo.Visible; }
            set { Titulo.Visible = value; }
        }
        //Agregado nuevo
        [Category("Autonomo properties")]
        public bool MaterialStyle
        {
            get { return material; }
            set { material = value; }
        }
        private void Titulo_Click(object sender, EventArgs e)
        {
            Texto.Focus();
        }

        #endregion
        #region Espacio: Icono
        [Category("Autonomo properties")]
        public DockStyle DockIcon // Para establecer la posición del icono
        {
            get { return Icono.Dock; }
            set
            {
                switch (value)
                {
                    case DockStyle.None:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Top:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Bottom:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    case DockStyle.Fill:
                        {
                            Icono.Dock = DockStyle.Left;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                    default:
                        {
                            Icono.Dock = value;
                            HelperControl.LocationTitle(Icono.Visible, Icono, Titulo);
                            LocationControl();
                            break;
                        }
                }
            }
        }
        private void LocationControl()
        {
            PlaceHolder.Location =
                new Point(Titulo.Location.X + 4, Titulo.Location.Y + 18);
        }

        [Category("Autonomo properties")]
        public Image ImageIcon // Establece la imagén del icono
        {
            get { return Icono.Image; }
            set { Icono.Image = value; Invalidate(); }
        }
        [Category("Autonomo properties")]
        public bool VisibleIcon
        {
            get { return Icono.Visible; }
            set
            {
                HelperControl.LocationTitle(value, Icono, Titulo);
                LocationControl();
                Icono.Visible = value;
            }
        }
        #endregion
        #region Espacio: Linea
        [Category("Autonomo properties")]
        public Color ColorLine // Cambiar el color de la Linea debajo del Texto
        {
            get { return Linea.BackColor; }
            set { Linea.BackColor = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorFocus // Cambiar el color de la Linea debajo del Texto
        {
            get { return focusColor; }
            set { focusColor = value; }
        }

        [Category("Autonomo properties")]
        public int SizeLine // Cambia la altura de la Linea
        {
            get { return Linea.Height; }
            set { Linea.Height = value; }
        }
        #endregion
        #region Espacio: Texto
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public override string Text // Muestra el contenido del TextBox
        {
            get { return Texto.Text; }
            set
            {
                value = value ?? string.Empty;
                Texto.Text = value;
            }
        }

        [Category("Autonomo properties")]
        public string Placeholder
        {
            get { return PlaceHolder.Text; }
            set { PlaceHolder.Text = value; }
        }
        /// <summary>
        /// TextId  almacenará información adicional al Text
        /// </summary>
        ///
        [Category("Autonomo properties")]
        public string TextId
        {
            get { return _idText; }
            set
            {
                value = value ?? string.Empty;
                if (value != _idText)
                { _idText = value; }
            }
        }
        [Category("Autonomo properties")]
        public CharacterCasing CharacterCasing //Cambia Mayuscula/Minuscula/Normal
        {
            get { return Texto.CharacterCasing; }
            set { Texto.CharacterCasing = value; }
        }
        [Category("Autonomo properties")]
        public char PasswordChar // Cambia el texto a modo Contraseña
        {
            get { return Texto.PasswordChar; }
            set { Texto.PasswordChar = value; }
        }
        [Category("Autonomo properties")]
        public int MaxLength // Establece el tamaño del contenido
        {
            get { return Texto.MaxLength; }
            set { Texto.MaxLength = value; }
        }
        [Category("Autonomo properties")]
        public Font FontText //Cambia el Tipo de fuente del TextBox
        {
            get { return Texto.Font; }
            set { Texto.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorText // Cambia el Color del TextBox
        {
            get { return Texto.ForeColor; }
            set { Texto.ForeColor = value; }
        }
        [Category("Autonomo properties")]
        public bool ReadOnly // Establece Solo lectura al TextBox
        {
            get { return Texto.ReadOnly; }
            set { Texto.ReadOnly = value; }
        }
        [Category("Autonomo properties")]
        public bool MultiLineText // Extiende el TextBox en varias lineas
        {
            get { return Texto.Multiline; }
            set { Texto.Multiline = value; }
        }
        [Category("Autonomo properties")]
        public ScrollBars ScrollBars // Muestra una barra de desplazamiento para el Multiline
        {
            get { return Texto.ScrollBars; }
            set { Texto.ScrollBars = value; }
        }
        [Category("Autonomo properties")]
        public System.Windows.Forms.HorizontalAlignment AlignText //Establece pa posición del contenido del TextBox
        {
            get { return Texto.TextAlign; }
            set { Texto.TextAlign = value; }
        }
        [Category("Autonomo properties")]
        public void Clear() // Limpia el TextBox
        {
            Texto.Clear();
        }
        [Category("Autonomo properties")]
        public new void Focus() //Selecciona el TextBox
        {
            Texto.Focus();
        }
        [Category("Autonomo properties")]
        public void SelectAll() // Selecciona el Contenido del TextBox
        {
            Texto.SelectAll();
        }

        [Category("Autonomo properties")]
        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                Contenedor.BackColor = value;
                Texto.BackColor = value;
                base.BackColor = value;
            }
        }

        //Eventos
        [Browsable(true)]
        [Category("Autonomo properties")]
        public event EventHandler TextBoxChanged; //TextBoxChanged puede ejecutar alguna función mientras cambie el valor del Texto
        public virtual void OnTextChanged()
        {
            if (TextBoxChanged != null)
                this.TextBoxChanged(this, EventArgs.Empty);
        }
        private void Texto_TextChanged(object sender, EventArgs e)
        {
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Texto.Text.Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
            }
            else
            {
                if (Texto.Text.Trim().Length > 0)
                    PlaceHolder.Visible = false;
                else
                    PlaceHolder.Visible = true;
            }
            this.OnTextChanged();
        }


        [Category("Autonomo properties")]
        public new event EventHandler<KeyEventArgs> KeyDown; // ejecuta alguna función mientras se presiona una tecla en especificco...
        protected override void OnKeyDown(KeyEventArgs e)
        {
            var handler = KeyDown;
            if (handler != null) handler(this, e);
        }
        private void Texto_KeyDown(object sender, KeyEventArgs e)
        {
            this.OnKeyDown(e);
        }
        [Category("Autonomo properties")]
        public new event EventHandler<KeyPressEventArgs> KeyPress; // Ejecuta alguna f.. al presionar la tecla.
        protected override void OnKeyPress(KeyPressEventArgs e)
        {
            var handler = KeyPress;
            if (handler != null) handler(this, e);
        }
        private void Texto_KeyPress(object sender, KeyPressEventArgs e)
        {
            this.OnKeyPress(e);
        }
        /// <summary>
        /// Evetno ButtonClick que ejecuta cieta instrucción 
        /// </summary>
        /// 
        [Category("Autonomo properties")]
        public event EventHandler ButtonClick;
        void button_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null) ButtonClick(sender, e);
        }
        #endregion
        private void Find_Focus(object sender, EventArgs e)
        {
            Linea.BackColor = focusColor;
            //Agregado nuevo
            if (MaterialStyle)
            {
                PlaceHolder.Visible = false;
                Titulo.Visible = true;
            }
        }
        private void Find_Leave(object sender, EventArgs e)
        {
            Linea.BackColor = lineMainColor;
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Texto.Text.Trim().Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    Titulo.Visible = false;
                }
            }
            else
            {
                if (Texto.Text.Trim().Length > 0)
                    PlaceHolder.Visible = false;
                else
                    PlaceHolder.Visible = true;
            }
        }

        private void PlaceHolder_Click(object sender, EventArgs e)
        {
            Texto.Focus();
        }

        private void FlatFindText_Load(object sender, EventArgs e)
        {
            //Nota: Si MaterialStyle está activo, el palceholder estará deshabilitado
            lineMainColor = Linea.BackColor;
            if (MaterialStyle)
            {
                PlaceHolder.Text = Titulo.Text;
                var len = Texto.Text.Trim().Length;
                if (len > 0)
                {
                    PlaceHolder.Visible = false;
                    Titulo.Visible = true;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    PlaceHolder.Text = Titulo.Text;
                    PlaceHolder.Font = Titulo.Font;
                    PlaceHolder.ForeColor = Titulo.ForeColor;
                    Titulo.Visible = false;
                }
            }
            else
            {
                if (Texto.Text.Trim().Length > 0)
                {
                    PlaceHolder.Visible = false;
                }
                else
                {
                    PlaceHolder.Visible = true;
                    PlaceHolder.ForeColor = Titulo.ForeColor;
                }
            }
        }
    }
}
