﻿namespace Autonomo.Control
{
    partial class CustomList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CustomList));
            this.Contenedor = new System.Windows.Forms.Panel();
            this._Stock = new System.Windows.Forms.Label();
            this.Linea = new System.Windows.Forms.Label();
            this._Cantidad = new System.Windows.Forms.NumericUpDown();
            this._stock_ = new System.Windows.Forms.Label();
            this.Icono = new System.Windows.Forms.PictureBox();
            this._Categoria = new System.Windows.Forms.Label();
            this._Titulo = new System.Windows.Forms.Label();
            this.Agregar = new System.Windows.Forms.Button();
            this.Error = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this._Cantidad)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Icono)).BeginInit();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.BackColor = System.Drawing.Color.White;
            this.Contenedor.Controls.Add(this.Error);
            this.Contenedor.Controls.Add(this.Agregar);
            this.Contenedor.Controls.Add(this._Stock);
            this.Contenedor.Controls.Add(this.Linea);
            this.Contenedor.Controls.Add(this._Cantidad);
            this.Contenedor.Controls.Add(this._stock_);
            this.Contenedor.Controls.Add(this.Icono);
            this.Contenedor.Controls.Add(this._Categoria);
            this.Contenedor.Controls.Add(this._Titulo);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 0);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(346, 68);
            this.Contenedor.TabIndex = 0;
            // 
            // _Stock
            // 
            this._Stock.AutoSize = true;
            this._Stock.Font = new System.Drawing.Font("Verdana", 11F, System.Drawing.FontStyle.Bold);
            this._Stock.Location = new System.Drawing.Point(115, 42);
            this._Stock.Name = "_Stock";
            this._Stock.Size = new System.Drawing.Size(46, 18);
            this._Stock.TabIndex = 4;
            this._Stock.Text = "0.00";
            // 
            // Linea
            // 
            this.Linea.BackColor = System.Drawing.Color.Gray;
            this.Linea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Linea.Location = new System.Drawing.Point(0, 66);
            this.Linea.Name = "Linea";
            this.Linea.Size = new System.Drawing.Size(346, 2);
            this.Linea.TabIndex = 7;
            // 
            // _Cantidad
            // 
            this._Cantidad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this._Cantidad.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this._Cantidad.DecimalPlaces = 2;
            this._Cantidad.Font = new System.Drawing.Font("Verdana", 14F);
            this._Cantidad.Location = new System.Drawing.Point(214, 36);
            this._Cantidad.Name = "_Cantidad";
            this._Cantidad.Size = new System.Drawing.Size(91, 26);
            this._Cantidad.TabIndex = 0;
            this._Cantidad.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this._Cantidad.ValueChanged += new System.EventHandler(this._Cantidad_ValueChanged);
            // 
            // _stock_
            // 
            this._stock_.AutoSize = true;
            this._stock_.Font = new System.Drawing.Font("Verdana", 9F);
            this._stock_.Location = new System.Drawing.Point(68, 44);
            this._stock_.Name = "_stock_";
            this._stock_.Size = new System.Drawing.Size(46, 14);
            this._stock_.TabIndex = 3;
            this._stock_.Text = "Stock:";
            // 
            // Icono
            // 
            this.Icono.Image = ((System.Drawing.Image)(resources.GetObject("Icono.Image")));
            this.Icono.Location = new System.Drawing.Point(3, 3);
            this.Icono.Name = "Icono";
            this.Icono.Size = new System.Drawing.Size(59, 60);
            this.Icono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Icono.TabIndex = 2;
            this.Icono.TabStop = false;
            // 
            // _Categoria
            // 
            this._Categoria.AutoSize = true;
            this._Categoria.Font = new System.Drawing.Font("Verdana", 9F);
            this._Categoria.ForeColor = System.Drawing.Color.Gray;
            this._Categoria.Location = new System.Drawing.Point(68, 21);
            this._Categoria.Name = "_Categoria";
            this._Categoria.Size = new System.Drawing.Size(76, 14);
            this._Categoria.TabIndex = 1;
            this._Categoria.Text = "Categories";
            // 
            // _Titulo
            // 
            this._Titulo.AutoSize = true;
            this._Titulo.Font = new System.Drawing.Font("Verdana", 10F, System.Drawing.FontStyle.Bold);
            this._Titulo.Location = new System.Drawing.Point(68, 3);
            this._Titulo.Name = "_Titulo";
            this._Titulo.Size = new System.Drawing.Size(116, 17);
            this._Titulo.TabIndex = 0;
            this._Titulo.Text = "Product name";
            // 
            // Agregar
            // 
            this.Agregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.Agregar.BackColor = System.Drawing.Color.Transparent;
            this.Agregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Agregar.FlatAppearance.BorderSize = 0;
            this.Agregar.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.Agregar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.Agregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Agregar.Image = ((System.Drawing.Image)(resources.GetObject("Agregar.Image")));
            this.Agregar.Location = new System.Drawing.Point(311, 33);
            this.Agregar.Name = "Agregar";
            this.Agregar.Size = new System.Drawing.Size(32, 32);
            this.Agregar.TabIndex = 1;
            this.Agregar.UseVisualStyleBackColor = false;
            // 
            // Error
            // 
            this.Error.AutoSize = true;
            this.Error.Font = new System.Drawing.Font("Verdana", 9F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Error.ForeColor = System.Drawing.Color.Salmon;
            this.Error.Location = new System.Drawing.Point(215, 19);
            this.Error.Name = "Error";
            this.Error.Size = new System.Drawing.Size(128, 14);
            this.Error.TabIndex = 8;
            this.Error.Text = "Stock Insuficiente";
            this.Error.Visible = false;
            // 
            // CustomList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.Controls.Add(this.Contenedor);
            this.Name = "CustomList";
            this.Size = new System.Drawing.Size(346, 68);
            this.Load += new System.EventHandler(this.CustomList_Load);
            this.Contenedor.ResumeLayout(false);
            this.Contenedor.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this._Cantidad)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Icono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.NumericUpDown _Cantidad;
        private System.Windows.Forms.Label _Stock;
        private System.Windows.Forms.Label _stock_;
        private System.Windows.Forms.PictureBox Icono;
        private System.Windows.Forms.Label _Categoria;
        private System.Windows.Forms.Label _Titulo;
        private System.Windows.Forms.Label Linea;
        public System.Windows.Forms.Button Agregar;
        private System.Windows.Forms.Label Error;
    }
}
