﻿namespace Autonomo.Control
{
    partial class FlatDateTime
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlatDateTime));
            this.Contenedor = new System.Windows.Forms.Panel();
            this.PlaceHolder = new System.Windows.Forms.Label();
            this.Drop = new System.Windows.Forms.Label();
            this.Texto = new System.Windows.Forms.MaskedTextBox();
            this.Titulo = new System.Windows.Forms.Label();
            this.arriba = new System.Windows.Forms.Label();
            this.abajo = new System.Windows.Forms.Label();
            this.izquierda = new System.Windows.Forms.Label();
            this.Linea = new System.Windows.Forms.Label();
            this.Icono = new System.Windows.Forms.Label();
            this.Date = new System.Windows.Forms.DateTimePicker();
            this.Wrong = new System.Windows.Forms.Label();
            this.Botton = new System.Windows.Forms.Label();
            this.Help = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.BackColor = System.Drawing.Color.Transparent;
            this.Contenedor.Controls.Add(this.PlaceHolder);
            this.Contenedor.Controls.Add(this.Drop);
            this.Contenedor.Controls.Add(this.Texto);
            this.Contenedor.Controls.Add(this.Titulo);
            this.Contenedor.Controls.Add(this.arriba);
            this.Contenedor.Controls.Add(this.abajo);
            this.Contenedor.Controls.Add(this.izquierda);
            this.Contenedor.Controls.Add(this.Linea);
            this.Contenedor.Controls.Add(this.Icono);
            this.Contenedor.Controls.Add(this.Date);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 0);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(278, 44);
            this.Contenedor.TabIndex = 0;
            // 
            // PlaceHolder
            // 
            this.PlaceHolder.AutoSize = true;
            this.PlaceHolder.BackColor = System.Drawing.Color.Transparent;
            this.PlaceHolder.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.PlaceHolder.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlaceHolder.ForeColor = System.Drawing.Color.DarkGray;
            this.PlaceHolder.Location = new System.Drawing.Point(46, 18);
            this.PlaceHolder.Name = "PlaceHolder";
            this.PlaceHolder.Size = new System.Drawing.Size(0, 16);
            this.PlaceHolder.TabIndex = 2;
            this.PlaceHolder.Click += new System.EventHandler(this.PlaceHolder_Click);
            // 
            // Drop
            // 
            this.Drop.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Drop.Dock = System.Windows.Forms.DockStyle.Right;
            this.Drop.Font = new System.Drawing.Font("Verdana", 10F);
            this.Drop.Image = ((System.Drawing.Image)(resources.GetObject("Drop.Image")));
            this.Drop.Location = new System.Drawing.Point(256, 18);
            this.Drop.Name = "Drop";
            this.Drop.Size = new System.Drawing.Size(22, 22);
            this.Drop.TabIndex = 0;
            this.Drop.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Drop.Click += new System.EventHandler(this.Drop_Click);
            // 
            // Texto
            // 
            this.Texto.BackColor = System.Drawing.Color.White;
            this.Texto.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.Texto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Texto.Font = new System.Drawing.Font("Verdana", 10F);
            this.Texto.Location = new System.Drawing.Point(39, 18);
            this.Texto.Name = "Texto";
            this.Texto.PromptChar = ' ';
            this.Texto.Size = new System.Drawing.Size(239, 17);
            this.Texto.TabIndex = 0;
            this.Texto.Click += new System.EventHandler(this.Texto_Click);
            this.Texto.TextChanged += new System.EventHandler(this.Texto_TextChanged);
            this.Texto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Texto_KeyDown);
            this.Texto.Leave += new System.EventHandler(this.Texto_Leave);
            // 
            // Titulo
            // 
            this.Titulo.AutoSize = true;
            this.Titulo.Font = new System.Drawing.Font("Verdana", 9F);
            this.Titulo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Titulo.Location = new System.Drawing.Point(36, 1);
            this.Titulo.Name = "Titulo";
            this.Titulo.Size = new System.Drawing.Size(156, 14);
            this.Titulo.TabIndex = 0;
            this.Titulo.Text = "Identificador del control";
            // 
            // arriba
            // 
            this.arriba.Dock = System.Windows.Forms.DockStyle.Top;
            this.arriba.Font = new System.Drawing.Font("Verdana", 10F);
            this.arriba.Location = new System.Drawing.Point(39, 0);
            this.arriba.Name = "arriba";
            this.arriba.Size = new System.Drawing.Size(239, 18);
            this.arriba.TabIndex = 0;
            // 
            // abajo
            // 
            this.abajo.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.abajo.Font = new System.Drawing.Font("Verdana", 10F);
            this.abajo.Location = new System.Drawing.Point(39, 40);
            this.abajo.Name = "abajo";
            this.abajo.Size = new System.Drawing.Size(239, 2);
            this.abajo.TabIndex = 0;
            // 
            // izquierda
            // 
            this.izquierda.Dock = System.Windows.Forms.DockStyle.Left;
            this.izquierda.Font = new System.Drawing.Font("Verdana", 10F);
            this.izquierda.Location = new System.Drawing.Point(36, 0);
            this.izquierda.Name = "izquierda";
            this.izquierda.Size = new System.Drawing.Size(3, 42);
            this.izquierda.TabIndex = 0;
            // 
            // Linea
            // 
            this.Linea.BackColor = System.Drawing.Color.Gray;
            this.Linea.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Linea.Font = new System.Drawing.Font("Verdana", 10F);
            this.Linea.Location = new System.Drawing.Point(36, 42);
            this.Linea.Name = "Linea";
            this.Linea.Size = new System.Drawing.Size(242, 2);
            this.Linea.TabIndex = 0;
            // 
            // Icono
            // 
            this.Icono.Dock = System.Windows.Forms.DockStyle.Left;
            this.Icono.Font = new System.Drawing.Font("Verdana", 10F);
            this.Icono.Image = ((System.Drawing.Image)(resources.GetObject("Icono.Image")));
            this.Icono.Location = new System.Drawing.Point(0, 0);
            this.Icono.Name = "Icono";
            this.Icono.Size = new System.Drawing.Size(36, 44);
            this.Icono.TabIndex = 0;
            // 
            // Date
            // 
            this.Date.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Date.Location = new System.Drawing.Point(0, 14);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(233, 20);
            this.Date.TabIndex = 0;
            this.Date.ValueChanged += new System.EventHandler(this.Date_ValueChanged);
            // 
            // Wrong
            // 
            this.Wrong.AutoSize = true;
            this.Wrong.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Wrong.ForeColor = System.Drawing.Color.Red;
            this.Wrong.Location = new System.Drawing.Point(36, 44);
            this.Wrong.Name = "Wrong";
            this.Wrong.Size = new System.Drawing.Size(0, 13);
            this.Wrong.TabIndex = 4;
            this.Wrong.TextChanged += new System.EventHandler(this.Wrong_TextChanged);
            // 
            // Botton
            // 
            this.Botton.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Botton.Location = new System.Drawing.Point(0, 44);
            this.Botton.Name = "Botton";
            this.Botton.Size = new System.Drawing.Size(278, 14);
            this.Botton.TabIndex = 5;
            // 
            // Help
            // 
            this.Help.AutoSize = true;
            this.Help.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Help.ForeColor = System.Drawing.Color.DarkGray;
            this.Help.Location = new System.Drawing.Point(36, 44);
            this.Help.Name = "Help";
            this.Help.Size = new System.Drawing.Size(0, 13);
            this.Help.TabIndex = 6;
            // 
            // FlatDateTime
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.Help);
            this.Controls.Add(this.Wrong);
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.Botton);
            this.Name = "FlatDateTime";
            this.Size = new System.Drawing.Size(278, 58);
            this.Load += new System.EventHandler(this.FlatDateTime_Load);
            this.Contenedor.ResumeLayout(false);
            this.Contenedor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.Label Titulo;
        private System.Windows.Forms.Label arriba;
        private System.Windows.Forms.Label abajo;
        private System.Windows.Forms.Label izquierda;
        private System.Windows.Forms.Label Linea;
        private System.Windows.Forms.Label Icono;
        private System.Windows.Forms.Label Drop;
        private System.Windows.Forms.MaskedTextBox Texto;
        private System.Windows.Forms.DateTimePicker Date;
        public System.Windows.Forms.Label Wrong;
        private System.Windows.Forms.Label Botton;
        public System.Windows.Forms.Label Help;
        private System.Windows.Forms.Label PlaceHolder;
    }
}
