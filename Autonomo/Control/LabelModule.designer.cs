﻿namespace Autonomo.Control
{
    partial class LabelModule
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LabelModule));
            this.Contenedor = new System.Windows.Forms.Panel();
            this.PanelText = new System.Windows.Forms.Panel();
            this.Description = new System.Windows.Forms.Label();
            this.Texto = new System.Windows.Forms.Label();
            this.PanelDown = new System.Windows.Forms.Panel();
            this.Izquierda = new System.Windows.Forms.Label();
            this.Arriba = new System.Windows.Forms.Label();
            this.Down = new System.Windows.Forms.PictureBox();
            this.Icono = new System.Windows.Forms.PictureBox();
            this.Contenedor.SuspendLayout();
            this.PanelText.SuspendLayout();
            this.PanelDown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Down)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Icono)).BeginInit();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.BackColor = System.Drawing.Color.Transparent;
            this.Contenedor.Controls.Add(this.PanelText);
            this.Contenedor.Controls.Add(this.PanelDown);
            this.Contenedor.Controls.Add(this.Icono);
            this.Contenedor.Controls.Add(this.Izquierda);
            this.Contenedor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Contenedor.Location = new System.Drawing.Point(0, 8);
            this.Contenedor.Name = "Contenedor";
            this.Contenedor.Size = new System.Drawing.Size(214, 36);
            this.Contenedor.TabIndex = 0;
            // 
            // PanelText
            // 
            this.PanelText.Controls.Add(this.Description);
            this.PanelText.Controls.Add(this.Texto);
            this.PanelText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelText.Location = new System.Drawing.Point(58, 0);
            this.PanelText.Name = "PanelText";
            this.PanelText.Size = new System.Drawing.Size(144, 36);
            this.PanelText.TabIndex = 0;
            // 
            // Description
            // 
            this.Description.Cursor = System.Windows.Forms.Cursors.Default;
            this.Description.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Description.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Description.ForeColor = System.Drawing.Color.Gray;
            this.Description.Location = new System.Drawing.Point(0, 19);
            this.Description.Name = "Description";
            this.Description.Size = new System.Drawing.Size(144, 17);
            this.Description.TabIndex = 3;
            this.Description.Text = "Description module";
            // 
            // Texto
            // 
            this.Texto.Cursor = System.Windows.Forms.Cursors.Default;
            this.Texto.Dock = System.Windows.Forms.DockStyle.Top;
            this.Texto.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Texto.Location = new System.Drawing.Point(0, 0);
            this.Texto.Name = "Texto";
            this.Texto.Size = new System.Drawing.Size(144, 17);
            this.Texto.TabIndex = 2;
            this.Texto.Text = "Title module";
            this.Texto.TextAlign = System.Drawing.ContentAlignment.BottomLeft;
            // 
            // PanelDown
            // 
            this.PanelDown.Controls.Add(this.Down);
            this.PanelDown.Cursor = System.Windows.Forms.Cursors.Default;
            this.PanelDown.Dock = System.Windows.Forms.DockStyle.Right;
            this.PanelDown.Location = new System.Drawing.Point(202, 0);
            this.PanelDown.Name = "PanelDown";
            this.PanelDown.Size = new System.Drawing.Size(12, 36);
            this.PanelDown.TabIndex = 4;
            // 
            // Izquierda
            // 
            this.Izquierda.Dock = System.Windows.Forms.DockStyle.Left;
            this.Izquierda.Location = new System.Drawing.Point(0, 0);
            this.Izquierda.Name = "Izquierda";
            this.Izquierda.Size = new System.Drawing.Size(10, 36);
            this.Izquierda.TabIndex = 5;
            // 
            // Arriba
            // 
            this.Arriba.Dock = System.Windows.Forms.DockStyle.Top;
            this.Arriba.Location = new System.Drawing.Point(0, 0);
            this.Arriba.Name = "Arriba";
            this.Arriba.Size = new System.Drawing.Size(214, 8);
            this.Arriba.TabIndex = 8;
            // 
            // Down
            // 
            this.Down.Cursor = System.Windows.Forms.Cursors.Default;
            this.Down.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Down.Image = ((System.Drawing.Image)(resources.GetObject("Down.Image")));
            this.Down.Location = new System.Drawing.Point(0, 20);
            this.Down.Name = "Down";
            this.Down.Size = new System.Drawing.Size(12, 16);
            this.Down.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Down.TabIndex = 3;
            this.Down.TabStop = false;
            // 
            // Icono
            // 
            this.Icono.Cursor = System.Windows.Forms.Cursors.Default;
            this.Icono.Dock = System.Windows.Forms.DockStyle.Left;
            this.Icono.Image = ((System.Drawing.Image)(resources.GetObject("Icono.Image")));
            this.Icono.Location = new System.Drawing.Point(10, 0);
            this.Icono.Name = "Icono";
            this.Icono.Size = new System.Drawing.Size(48, 36);
            this.Icono.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.Icono.TabIndex = 0;
            this.Icono.TabStop = false;
            // 
            // LabelModule
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.Contenedor);
            this.Controls.Add(this.Arriba);
            this.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "LabelModule";
            this.Size = new System.Drawing.Size(214, 44);
            this.Contenedor.ResumeLayout(false);
            this.PanelText.ResumeLayout(false);
            this.PanelDown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Down)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Icono)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel Contenedor;
        private System.Windows.Forms.PictureBox Icono;
        private System.Windows.Forms.Panel PanelDown;
        private System.Windows.Forms.PictureBox Down;
        private System.Windows.Forms.Label Izquierda;
        private System.Windows.Forms.Panel PanelText;
        private System.Windows.Forms.Label Description;
        private System.Windows.Forms.Label Texto;
        private System.Windows.Forms.Label Arriba;
    }
}
