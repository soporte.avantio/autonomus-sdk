﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public class CustomButton : Button
    {
        public CustomButton()
        {
            base.FlatStyle = FlatStyle.Flat;
            base.FlatAppearance.BorderSize = 0;
            base.Font = new System.Drawing.Font("Verdana", 10, System.Drawing.FontStyle.Regular);
            base.TextImageRelation = TextImageRelation.ImageBeforeText;
            base.Cursor = Cursors.Hand;
        }
    }
}
