﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public partial class CustomList : UserControl
    {
        public int IdProducto { get; set; }     
        public CustomList()
        {
            InitializeComponent();
        }
        #region Espacio: Linea
        public Color ColorLine // Cambiar el color de la Linea debajo del Texto
        {
            get { return Linea.BackColor; }
            set { Linea.BackColor = value; }
        }
        public int SizeLine // Cambia la altura de la Linea
        {
            get { return Linea.Height; }
            set { Linea.Height = value; }
        }
        #endregion

        public Image Imagen { get { return Icono.Image; } set { Icono.Image = value; } }

        public string Titulo { get { return _Titulo.Text; } set { _Titulo.Text = value; } }
        public string Category { get { return _Categoria.Text; } set { _Categoria.Text = value; } }
        public string Stock { get { return _Stock.Text; } set { _Stock.Text = value; } }
        public decimal Cantidad { get { return decimal.Parse(_Cantidad.Value.ToString()); } set { _Cantidad.Value = value; } }
        public bool Flag 
        {
            get 
            {
                decimal stock = decimal.Parse(_Stock.Text);
                decimal cantidad = decimal.Parse(_Cantidad.Value.ToString());
                var value = (cantidad > stock ? false : true);
                return value;
            }
        }
        private void CustomList_Load(object sender, EventArgs e)
        {
            Autonomo.Class.RoundObject.RoundUControl(this, 10, 10);
        }

        private void _Cantidad_ValueChanged(object sender, EventArgs e)
        {
            Error.Visible = !Flag;
        }
    }
}
