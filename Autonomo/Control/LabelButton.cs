﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Autonomo.Control
{
    public partial class LabelButton : UserControl
    {
        public LabelButton()
        {
            InitializeComponent();
            Down.Click += new EventHandler(button_Click);
            Texto.Click += new EventHandler(button_Click);
            PanelDown.Click += new EventHandler(button_Click);
            Description.Click += new EventHandler(button_Click);

            Down.MouseEnter += new EventHandler(Customer_MouseEnter);
            Down.MouseLeave += new EventHandler(Customer_MouseLeave);
            Texto.MouseEnter += new EventHandler(Customer_MouseEnter);
            Texto.MouseLeave += new EventHandler(Customer_MouseLeave);
            PanelDown.MouseEnter += new EventHandler(Customer_MouseEnter);
            PanelDown.MouseLeave += new EventHandler(Customer_MouseLeave);
            Description.MouseEnter += new EventHandler(Customer_MouseEnter);
            Description.MouseLeave += new EventHandler(Customer_MouseLeave);
            PanelText.MouseEnter += new EventHandler(Customer_MouseEnter);
            PanelText.MouseLeave += new EventHandler(Customer_MouseLeave);
            Down.Visible = false;
        }
        private void Customer_MouseEnter(object sender, EventArgs e)
        {
            Texto.Font = new Font(Texto.Font, FontStyle.Bold);
            Description.Font = new Font(Description.Font, FontStyle.Bold);
            Down.Visible = true;
        }
        private void Customer_MouseLeave(object sender, EventArgs e)
        {
            Texto.Font = new Font(Texto.Font, FontStyle.Regular);
            Description.Font = new Font(Description.Font, FontStyle.Regular);
            Down.Visible = false;
        }
        [Category("Autonomo properties")]
        public override Color BackColor { get => base.BackColor; set => base.BackColor = value; }

        #region Espacio: Icono
        [Category("Autonomo properties")]
        public Image ImageDown
        {
            get { return Down.Image; }
            set { Down.Image = value; Invalidate(); }
        }
        #endregion

        #region Espacio: Texto
        [EditorBrowsable(EditorBrowsableState.Always)]
        [Browsable(true)]
        [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
        [Bindable(true)]
        [Category("Autonomo properties")]
        public override string Text
        {
            get { return Texto.Text; }
            set
            {
                value = value ?? string.Empty;
                Texto.Text = value;
            }
        }
        [Category("Autonomo properties")]
        public Font FontText
        {
            get { return Texto.Font; }
            set { Texto.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorText
        {
            get { return Texto.ForeColor; }
            set { Texto.ForeColor = value; }
        }

        [Category("Autonomo properties")]
        public string DetailText
        {
            get { return Description.Text; }
            set
            {
                value = value ?? string.Empty;
                Description.Text = value;
            }
        }
        [Category("Autonomo properties")]
        public Font FontDetailText
        {
            get { return Description.Font; }
            set { Description.Font = value; }
        }
        [Category("Autonomo properties")]
        public Color ColorDetailText
        {
            get { return Description.ForeColor; }
            set { Description.ForeColor = value; }
        }
        #endregion

        #region Espacio: Eventos
        [Category("Autonomo properties")]
        public event EventHandler ButtonClick;
        void button_Click(object sender, EventArgs e)
        {
            if (ButtonClick != null) ButtonClick(sender, e);
        }

        #endregion
    }
}
